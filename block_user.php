<?php
ini_set('display_errors', 1);
// include init class
require('./class/init.php'); 
//Create object for redirect class
 $redirect = $init->getRedirect();
 //Create object for object class
 $users = Users::getInstance();
  //Create object for admin  class
 $admin = Admin::getInstance();

  //getting Username from SESSION
 $session = $init->getSession();
 $session->startSession();

 //check session  
    if(!$session->__get("username"))
    {
          $redirect->redirect("index.php");
    }
    if($session->__get("userType")==0)
    {
          $redirect->redirect("index.php");
    }
 $username=$_SESSION['username']; 
 $limit=6;
 //calling function for selecting userId from users table 
 $arrayId=$users->selectUserId($username);

 //calling function for selecting allPray from praytb and countPray table for login user
 $id=$arrayId['id'];
 $prays=$users->allPrayLoginUser($id,$limit);
  
 //calling function of changeImage of login user update
 $updateImage=$users->updateprofile($id);
 //echo "<pre>";print_r($updateImage);die("dff");
 //echo $updateImage['imageSrc']; die("///h");

 //create object for session class
 $session=$init->getSession();
 //Starting Session
 //$session->startSession();
 if($session->__get("limit"))
 {
  $session->__unset("limit");   
 }
 // get all block users
 $where=array('block'=>'1');
  $data=$users->selectData('Users','*',$where);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
           <?php include('include/inc.link.php');?>
        <!--start popup for change user profile-->
      
         <!--include data table file-->

          <?php include('include/inc.link-profile.php');?>
    
        <style>
        .user_pic a:hover {
        color:#fff;
        background:#000 !important;
        }
        .user_pic a {
        background:transparent !important;
        }
        .logo a {outline:none; text-decoration:none !important; border:none;}
        </style>

        <!--include data table file-->

   
    </head>
    <body> 
    <?php 
        include('include/header.php');
        include('include/top_banner_home.php');
    ?>


    <div class="container">
        <div class="">
           <div class="profile_box">
        <div class="timeline_big_pic">
        
            <img  src="images/timeline_pic.jpg" alt="" />
        </div>
        <div class="user_details">
            <div class="user_pic">  

                <a href="#link1" title="" class="link" data-toggle="modal">Change profile picture</a>
                <img class="changeimage" style="max-width:160px;" src="<?php if(!empty($updateImage['imageSrc']))echo $updateImage['imageSrc'];else echo "uploads/user_pic.jpg"; ?>"  alt="" />
            </div>
            <div class="user_disc">
            <div class="user_name">
                <?php /* if($_SESSION['username']){?>
                <p><a href="#"><?php echo $username; ?></a> <span>Priest</span></p>
                <?php } else{?>
                <p><a href="#"><?php echo "Guest"; ?></a> <span>Priest</span></p>
                <?php } */?>
            </div>
            <div class="clear"></div>
            <div class="btn_profile">
               <?php include('include/nav.php');?>
            </div>
            </div>
        </div>
        <div class="clear"></div>
        </div>
        </div>
        </div>
         <div class="profile_box">

    <div class="container">
    <div class="row">
        <div class="">
            <div class="panel panel-default">
                <div class="panel-body">
                     <script>
       jQuery(document).ready(function()
        {
            var remove_id;
            $(".remove").click(function(e)
            {
                if (confirm("Are you sure you want to Delete")) {
                    remove_id=this.id;
                    var info = 'remove=' + remove_id;
                    $.ajax({
                        type: "POST",
                        url: "ajaxCall.php",
                        data: info,
                        success: function(data)
                        {
                        
                        $("#"+remove_id+"").parent().parent().remove();
                        alert("Recorde Delete successfully");
                        }
                    });
                }
            });
           /* $(".block").click(function(e)
            {
                if (confirm("Are you sure you want to Block User")) {
                        var msg = prompt("Please enter your Message");
                        var txt = $.trim( msg );
                        if(txt){
                        block=this.id;
                        var info = 'block=' + block;
                        $.ajax({
                            type: "POST",
                            url: "ajaxCall.php",
                            data: { block: block, message: msg },
                            success: function(data)
                            {
                                $("#blockrow"+block+"").replaceWith(data);
                                alert("User Block successfully");
                            }
                        });
                    }
                }
            });*/
            $(".unblock").click(function(e)
            {
                if (confirm("Are you sure you want to UnBlock User")) {
                    unblock=this.id;
                    var info = 'unblock=' + unblock;
                    $.ajax({
                        type: "POST",
                        url: "ajaxCall.php",
                        data: info,
                        success: function(data)
                        {
                        //$("#blockrow"+unblock+"").replaceWith(data);
                        $("#"+unblock+"").parent().parent().remove();
                        alert("User UnBlock successfully");
                        }
                    });
                }
            });
        });
    </script>
                    <script type="text/javascript">
                        jQuery(document).ready(function($)
                        {
                            $("#example-1").dataTable({
                                aLengthMenu: [
                                    [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
                                ]
                            });
                        });
                    </script>

                    <table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Delete</th>
                                <th>Block</th>
                            </tr>
                        </thead>
                    <!--
                        <tfoot>
                            <tr>
                                <th>user</th>
                                <th>Position</th>
                                <th>Office</th>
                                <th>Age</th>
                                <th>Start date</th>
                                <th>Salary</th>
                            </tr>
                        </tfoot>
                    -->
                        <tbody id="blockUser">
                            <?php foreach($data as $row){
                                ?>
                            <tr id="blockrow<?php echo $row['id'];?>">
                                <td><?php echo $row['id']?></td>
                                <td><a href="adminprayer.php?userinfo=<?php echo $row['id']?>" class="adminusername"><?php echo $row['username']?></a></td>
                                <td><?php echo $row['email']?></td>
                                <td><a id="<?php echo $row['id'];?>" class="btn btn-danger remove" values="<?php echo $row['id'];?>" data-toggle="modal" role="button" href="#">Delete</a></td>
                                <?php if($row['block']==1){?>

                                <td ><a id="unblock_<?php echo $row['id'];?>" class="btn btn-success unblock" values="<?php echo $row['id'];?>" data-toggle="modal" role="button" href="#">Unblock</a></td>
                                <?php } ?>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                  </div>
            </div>

        </div>
        <!--start change image popup div-->
        <div>
            <div id="link1" class="modal fade bs-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                         <div class="modal-header ">
                            <!--start uploadImage Form-->
                            <form id="uploadImage" class="forminfo" action="" method="post" enctype="multipart/form-data">
                                <div id="selectImage">
                                    <label>Select Your Image</label><br/>
                                    <input type="file" name="file" id="file" required />
                                    <input type="hidden" name="user_id" value="<?php echo $id;?>"/>
                                    <input type="submit" name="uploadImage" value="Upload" class="primarybtn submit" />
                                </div>
                            </form>
                            <!--End uploadImage Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <!--End change image popup div-->  
    </div>
    <?php include('include/footer.php');?>
    <script src="js/ajaxCall.js"></script>
    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
    <!-- Bottom Scripts -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/TweenMax.min.js"></script>
    <script src="assets/js/resizeable.js"></script>
    <script src="assets/js/joinable.js"></script>
    <script src="assets/js/xenon-api.js"></script>
    <script src="assets/js/xenon-toggles.js"></script>
    <script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
    <!-- Imported scripts on this page -->
    <script src="assets/js/datatables/dataTables.bootstrap.js"></script>
    <script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
    <script src="assets/js/datatables/tabletools/dataTables.tableTools.min.js"></script>


    <!-- JavaScripts initializations and stuff -->
    <script src="assets/js/xenon-custom.js"></script>

   
    </body>
</html>

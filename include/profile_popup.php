 <div>
    <div id="link1" class="modal fade bs-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                 <div class="modal-header ">
                    <!--start uploadImage Form-->
                    <form id="uploadImage" class="forminfo" action="" method="post" enctype="multipart/form-data">
                        <div id="selectImage">
                            <label>Select Your Image</label><br/>
                            <input type="file" name="file" id="file" required />
                            <input type="hidden" name="user_id" value="<?php echo $id;?>"/>
                            <input type="submit" name="uploadImage" value="Upload" class="primarybtn submit" />
                        </div>
                    </form>
                    <!--End uploadImage Form-->
                </div>
            </div>
        </div>
    </div>
</div>
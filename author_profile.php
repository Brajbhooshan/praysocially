<?php
ini_set('display_errors', 1);
// include init class
require('./class/init.php'); 
//Create object for redirect class
 $redirect = $init->getRedirect();
 //Create object for object class
 $users = Users::getInstance();
  //getting Username from SESSION
 $session = $init->getSession();
 $session->startSession();
//Session For author
 $session->__set("author_id",$_GET['id']);

 //check session  
    
 $limit=6;
if(isset($_GET['id']))
{
    $prays=$users->viewMyPray($_GET['id'],$limit);
  //echo '<pre>';print_r($prayer);die;
}else{
    $redirect->redirect("index.php");
}
 $session=$init->getSession();
 //Starting Session
 if($session->__get("limit"))
 {
  $session->__unset("limit");   
 }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <?php include('include/inc.link.php');?>
        <!--start popup for change user profile-->
        <link href="popup/css/bootstrap.css" rel="stylesheet" type="text/css">
 
        <script src="popup/js/bootstrap.min.js"></script>
    
        <style>
        .user_pic a:hover {
        color:#fff;
        background:#000 !important;
        }
        .user_pic a {
        background:transparent !important;
        }
        .logo a {outline:none; text-decoration:none !important; border:none;}
        </style>
    </head>

    <body> 
    	<?php 
    include('include/header.php');
    include('include/top_banner_home.php');
    ?>
    <div class="header">
        
    <div class="container">
    <div class="row">
    <div class="">
    	<div class="profile_box">
    	<div class="timeline_big_pic">
    	
        	<img  src="images/timeline_pic.jpg" alt="" />
        </div>
        <div class="user_details">
        	<div class="user_pic">  
                <img class="changeimage" style="max-width:160px;" src="<?php if(!empty($updateImage['imageSrc']))echo $updateImage['imageSrc'];else echo "uploads/user_pic.jpg"; ?>"  alt="" />
            </div>
         
        </div>
        <div class="clear"></div>
        </div>
        <div class="clear"></div>
        </div>
        </div>
    	<div class="prayer_box">
        	<ul id="loadMoreAuthor" class="loadMoreAuthorClass">
                <!-- show allPray which is pray by login user-->
                <?php if(!empty($prays)){ foreach ($prays as $pray){ ?>
            	<li>
                	<div class="prayer_content">
                        <p><?php if(strlen($pray['title'])<30) echo $pray['title']; else { $title=substr($pray['title'],0,30); echo $title."....";} ?></p>    
                        <a href="showPray.php?id=<?php echo $pray['id']; ?> ">Read more</a>
                    </div>
                   <?php if($session->__get("username")) {?>

                        <div class="prayer_counter_box">
                                 <div class="praycount_left">
                                    <img src="images/hand.png" alt="" />
                                    <p><span class="single_pray" id="<?php echo $pray['id'];?>"><?php echo $count=$users->countPray($pray['id']);?> </span>Prayers</p>
                                </div>
                                <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                    <p class="creator_name"><?php $author=$users->author($pray['user_id']); ?>
                                       <a href ="author_profile.php?id=<?php echo $author[0]['id']; ?>" >

                                                <?php echo $author[0]['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <div class="prayer_btn">
                            <a class="Pray like_prayer" id="<?php echo $pray['id'] ?>"  href="javascript:void(0);" value="<?php echo $pray['id'] ?>">+1 Prayer</a>
                            <a href="javascript:void(0);" class="share_fb"><img src="images/fshare.jpg" alt="" /></a>
                            <div class="clear"></div>
                        </div>
                        <? } else{?>
                         <div class="prayer_counter_box">
                                 <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                    <p class="creator_name"><?php $author=$users->author($pray['user_id']); ?>
                                       <a href ="author_profile.php?id=<?php echo $author[0]['id']; ?>" >

                                                <?php echo $author[0]['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <? }?>
                </li>
               <?php } }?>
            </ul>
            <div class="clear"></div>
            <div class="load_more">
                <a href="javascript:void(0);" class="loadMoreAuthor"><i class="fa fa-refresh"></i> Load More</a>
            </div>
        </div>
        <!--start change image popup div-->
        <div>
            <div id="link1" class="modal fade bs-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                         <div class="modal-header ">
                            <!--start uploadImage Form-->
                            <form id="uploadImage" class="forminfo" action="" method="post" enctype="multipart/form-data">
                                <div id="selectImage">
                                    <label>Select Your Image</label><br/>
                                    <input type="file" name="file" id="file" required />
                                    <input type="hidden" name="user_id" value="<?php echo $id;?>"/>
                                    <input type="submit" name="uploadImage" value="Upload" class="primarybtn submit" />
                                </div>
                            </form>
                            <!--End uploadImage Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <!--End change image popup div-->  
    </div>
    <?php include('include/footer.php');?>
    <script src="js/ajaxCall.js"></script>
    </body>
</html>
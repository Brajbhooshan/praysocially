<?php
session_start();
require('class/init.php');

    $redirect   = $init->getRedirect();
    $users      = Users::getInstance();
    $mail       = eMail::getInstance();
    $comments   = Comments::getInstance();
    $session    = $init->getSession();
    $session->startSession();

    
    /*check session*/  

if((!$session->__get("username")) || (!isset($_REQUEST['commentId'])))
{   
    if(isset($_REQUEST['action']) && !empty($_REQUEST['action']) && ($_REQUEST['action'] == 'editComments')) {
        
        $session->__set("editComments","true");
        $session->__set("prayId",$_REQUEST['id']);
        $session->__set("encrypt",$_REQUEST['encrypt']);
        $session->__set("editCommentsId",$_REQUEST['commentId']);
        $redirect->redirect("signin.php");
    }
    else{
        
        $redirect->redirect("index.php");
    }
}

    /* calling function for Pray */

        $pray        = $users->showPrayByid($_GET['id']);
        $prayerUid   = $comments->prayerUser($pray['user_id']);
        
    /*  
        Function calling for comments
        Create   :   piyush
        Date     : 25/07/2015
    
    */

if(isset($_POST['postcomment']))
{
    /*check post comment empty or not*/
    if (isset($_POST['notification'])) {

        if(isset($_POST['comment']) && !empty($_POST['comment'])){

           $array          = array('id'=>$_GET['commentId'],'comments'=>$_POST['comment'],'prayId'=>$_GET['id'],'encrypt'=>$_GET['encrypt'],'email' =>$_POST['useremail']);     
           $result         = $mail->confirmationMail($array);
           $response       = $comments->updateBlockComment($array);

            }
        else{

            $errorMessage  = "Please Enter Comment";
        }
    }
    else{

        $errorMessage      = "Please check the checkBox first befor submit updated comments !";
    }    
}

/*

Function : For Block/UnBlock Prayers 
Create   : Braj
Date     : 29/07/2015   

*/

if(isset($_GET['commentId']))
{   
    $editcommentdata      = $comments->selectComments($_GET['commentId']);
    //print_r($updatecomment);die('dd');
     
}
if (($session->__get("id")==$pray['user_id']) && ($session->__get("id")==$editcommentdata[0]['commentsUserId'])) { $redirect->redirect("index.php"); }

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head> <?php include('include/inc.link.php');?></head>
<body>
<?php 
    include('include/header.php');
    include('include/top_banner_home.php');
?>

<div class="container">
    <div class="prayer_box">
        <?php 
                /* Start If Login user No longer show Block prayer*/

                if(isset($selectBlockPray) && !empty($selectBlockPray) || is_array($selectBlockPray) && !empty($selectBlockPray)){echo "YOU ARE BLOCK FOR THIS PRAYER BY PRAY CREATER USER !";}else{?>

                    <div class="prayer_content">
                    <p><b>Title </b>:- <?php echo $pray['title'];?></p>
                    <p><b>Author's name </b>:- <?php echo $prayerUid[0]['username']; ?></p>
                    <br>
                    <p><b>Pray </b>:- <?php echo $pray['pray'];?></p>

                    </div>
                    <?php if(isset($_SESSION['username'])) {?>

                    <div class="prayer_counter_box">
                        <img src="images/hand.png" alt="" />
                        <p><span class="single_pray" id="<?php echo $pray['id'];?>"><?php echo $count=$users->countPray($pray['id']);?> </span>Prayers</p>
                    </div>
                    <div class="prayer_btn">
                        <a class="Pray like_prayer" id="<?php echo $pray['id'] ?>"  href="#" value="<?php echo $pray['id'] ?>">+1 Prayer</a>
                        <a href="#" class="share_fb"><img src="images/fshare.jpg" alt="" /></a>
                        <div class="clear"></div>

                    </div>
                    <? } ?>

        <div class="clear"></div>
    </div>
       <?php if($session->__get("username")) {?>
            <div class="prayer_box">
              <?php 
                    /* 
                        add comment form[started here]
                        added by piyush
                        25/07/2015
                    */

                if(!empty($errorMessage))
                    echo $errorMessage;
                ?>
                    <!--
                        Add block/unblock button
                        Added By : Braj
                        Date     : 27/07/2015 
                    -->
                <form method="post" class="commentform">
                    <input type="hidden" name="useremail" id="useremail" value="<?php echo $prayerUid[0]['email']?>">
                    <textarea name="comment"  placeholder="Enter Your Comments" required=""><?php if(isset($editcommentdata) && !empty($editcommentdata) ){ echo $editcommentdata[0]['comments'];}?></textarea>
                &nbsp&nbsp&nbsp&nbsp&nbsp<input type="checkbox" name="notification" class="notification" value="">&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp! Checked this to Send notification , comments updated !  <br>
                    <input name="postcomment" value="comment" type="submit" class="" />
                </form>
            </div>
        <?php }?>
    <div class="prayer_box">
            <?php if (is_array($comment) && !empty($comment) || is_object($comment) && !empty($comment)){foreach ($comment as $row){ ?>
                <div class="prayer_content">
                    <form method="post" >
                        <input type="hidden" name="commentsID" value="<?php echo $row['id']; ?>"/>
                        <input type="hidden" name="blockuserId" value="<?php echo $row['commentsUserId']; ?>" />
                        <p><b>Comments </b>:- <?php echo $row['comments'];?></p>
                        <p><b>User's name </b>:- <?php $username=$comments->selectCommentsUserNameById($row['commentsUserId']);if (is_array($username) || is_object($username)){foreach ($username as $uname) {
                              echo $uname['username'];
                              ?><input type="hidden" name="email" value="<?php echo $uname['email'] ?>" />
                              <input type="hidden" name="prayTitle" value="<?php echo $pray['title'] ?>" /><?php

                            }}else{continue;}?></p>
                        <br>

                        </div>
                    </form>
                  <?php }}else{}
                 } /*End Pray Hide else Block*/ ?>

            <div class="clear"></div>
        
    </div>
</div>
<?php include('include/footer.php');?>
 <script src="js/ajaxCall.js"></script>

</body>
</html>

<?php
require('class/init.php'); 
    
    $redirect    = $init->getRedirect();
    $users       = Users::getInstance();
    $session     =$init->getSession();
 
    /* Starting Session */
    $session->startSession();
    
    $updateImage = $users->updateprofile($session->__get("id"));

    if(!$session->__get("username"))
    {
        $redirect->redirect("index.php");
    }

    $id        = $_SESSION['id'];

    /* calling function for all Followers */
    
    $Followers = $users->Follower($id);

    /* create object for session class */

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
       
        <?php include('include/inc.link.php');?>
        
        <!--start popup for change user profile-->
        
        <?php include('include/inc.link-profile.php');?>
        
        <style>
        
            .user_pic a:hover {
            color:#fff;
            background:#000 !important;
            }
            .user_pic a {
            background:transparent !important;
            }
            .logo a {outline:none; text-decoration:none !important; border:none;}
        
        </style>
    </head>
 
    <body> 
        <?php 
            include('include/header.php');
            include('include/top_banner_home.php');
            include('profile.newheader.php');
        ?>     
        <div class="container">
        <div class="row">
        <div class="">
            <div class="profile_box">
           
            <div class="clear"></div>
            </div>
            </div>
            <div class="">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <script type="text/javascript">
                        jQuery(document).ready(function($)
                        {
                            $("#example-1").dataTable({
                                aLengthMenu: [
                                    [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
                                ]
                            });
                        });
                        </script>
                        <table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Serial No</th>
                                    <th>Follower</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($Followers)){$i=1;
                                foreach ($Followers as $value):?>
                                    <tr>
                                        <td>
        	                                <?php echo $i;?>
                      					</td>
                                        <td>
                                            <a href ="author_profile.php?id=<?php echo $value['user_id']; ?>" ><?php $follower_name=$users->userdetail($value['user_id']);if($follower_name){echo $follower_name[0]['username'];}?></a>
                                        </td>
        						    </tr>
                                <?php $i++; endforeach; } ?> 
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <?php include'include/profile_popup.php';?>
            </div>
            
        </div>
        <?php include('include/footer.php');?>
        <script src="js/ajaxCall.js"></script>
        <link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/TweenMax.min.js"></script>
        <script src="assets/js/resizeable.js"></script>
        <script src="assets/js/joinable.js"></script>
        <script src="assets/js/xenon-api.js"></script>
        <script src="assets/js/xenon-toggles.js"></script>
        <script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
    	<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
        <script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
        <script src="assets/js/datatables/tabletools/dataTables.tableTools.min.js"></script>
    	<script src="assets/js/xenon-custom.js"></script>
    </body>
</html>
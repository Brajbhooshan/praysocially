<?php
ini_set('display_errors', 1);

require('./class/init.php'); 
  
  $redirect     = $init->getRedirect();
  $users        = Users::getInstance();
  $comments     = Comments::getInstance();
  $session      = $init->getSession();
  $session->startSession();

  /* check session */  
  
if(!$session->__get("username"))
{
  $redirect->redirect("index.php");
}
 
  $username     = $_SESSION['username']; 
  $limit        = 6;
  
  /* calling function for selecting userId from users table */ 
  
  $arrayId      = $users->selectUserId($username);
  $id           = $arrayId['id'];
  $user_details = $users->userdetail($id);
  $user_pwd     = $user_details['0']['password'];
  $prays        = $users->allPrayLoginUser($id,$limit);
  $updateImage  = $users->updateprofile($id);
  //print_r($updateImage);die('gggggg');
  
if($session->__get("limit"))
{
$session->__unset("limit");   
}

/* update login user profile */

if(isset($_POST['update']))
{  

  if(isset ($_POST['old_pwd']) && !empty($_POST['old_pwd']) && isset($_POST['new_pwd']) && !empty($_POST['new_pwd']) && isset($_POST['cnfrm_new_pwd']) && !empty($_POST['cnfrm_new_pwd']))
  {
    /* if password and confirm password is match */

    if($_POST['new_pwd']==$_POST['cnfrm_new_pwd']){

        $password = md5($_POST['new_pwd']);
        $array    = array('id'=>$session->__get("id"),'password'=>$password);
        $update   = $users->updateChangePassword($array);
      
      if($update==1)
      {
        ?><script>alert("Password updated successfully done !!!");window.location.assign('edit_profile.php');</script><?php   
      }
    }
  }
  else{

    ?><script>alert("Please fill currect entry !");</script><?php
  }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>:: Praysocially ::</title>
    <link href="css/style.css" type="text/css" rel="stylesheet" />
    <link href="css/responsive.css" type="text/css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,400italic,500,500italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <!--start popup for change user profile-->
    <link href="popup/css/bootstrap.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
    <script src="popup/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
    <script type="text/javascript">
      $(document).ready(function() {
          //hide green chck for chcking username in database 
          $('.text_success').hide();
          //Here Chcking signup form validation iff all fields remain Empty
          $(".updatefrm").validate({
              rules:
              {
                username :{
                required:true,
                maxlength: 25
                },
                old_pwd :{
                required:true,
                
                },
                new_pwd :{
                required:true,
                maxlength: 25
                },
                cnfrm_new_pwd:{
                equalTo: "#new_pwd"
                },
              },
              messages:
              {
                //email:"Enter Email Address",
                password :{
                required:" Enter Password",
                },
                confirm_password :"Password does not match, Please try again",
              }
          });

          $('.old_pwd').blur(function (e) { 
            e.preventDefault();
            var data = {};
            data.password=$(".old_pwd").val();
            $.ajax({
              type: "POST",
              url: "ajaxRegister.php",
              data: data,
              //cache: false,
              success: function (response) { var json = $.parseJSON(response);
                //alert(response);
                if(json=="failled"){ alert("Password does not match ! Please Enter currect");
                    $(".old_pwd").val("");
                    $(".old_pwd").focus();
                    $('.text_success').hide();
                    $('#label1').show();
                  }
                  else if(response=='"emptyname"'){
                    
                  $('.text_success').hide();
                  
                }
                else{
                  
                    $('#label1').hide();
                    $('.text_success').show();
                 }
              }
            }); 
            return false;
         });
        
      });
      </script>
        <style>
        .user_pic a:hover {
        color:#fff;
        background:#000 !important;
        }
        .user_pic a {
        background:transparent !important;
        }
        .logo a {outline:none; text-decoration:none !important; border:none;}
        </style>
        <script src="js/jquery.validate.js"></script>
    </head>

    <body> 
    	<?php 
    include('include/header.php');
    include('include/top_banner_home.php');

    ?>
    <div class="header">
        
    <div class="container">
    <div class="row">
    <div class="">
    	<div class="profile_box">
            	<div class="timeline_big_pic">
            	
                	<img  src="images/timeline_pic.jpg" alt="" />
                </div>
                <div class="user_details">
                        	<div class="user_pic">  

                                <a href="#link1" title="" class="link" data-toggle="modal">Change profile picture</a>
                                <img class="changeimage" style="max-width:160px;" src="<?php if(!empty($updateImage['imageSrc']))echo $updateImage['imageSrc'];else echo "uploads/user_pic.jpg"; ?>"  alt="" />
                            </div>
                            <div class="user_disc">
                                <div class="user_name">
                                    <?php if($_SESSION['username']){?>
                                	<p><a href="#"><?php echo $username; ?></a> <span>Priest</span></p>
                                    <?php } else{?>
                                    <p><a href="#"><?php echo "Guest"; ?></a> <span>Priest</span></p>
                                    <?php }?>
                                </div>
                                <div class="clear">
                                    
                                </div>
                                
                            </div>
                </div>
                <div class="clear">
                    
                </div>
        </div>
        <div class="clear"></div>
        </div>
        </div>
    	<div class="prayer_box">
        <div class="">
            <div class="profile_sign_up form_box">
                <h2>Update Your account!</h2>
               <?php foreach($user_details as $key => $user) {
               ?>
               <form method="post" id="updatefrm" action="" class="updatefrm">
                    <div class="form_inr">
                      <div class="form_field">
                      <input type="password" name="old_pwd"  id="old_pwd" class="old_pwd" placeholder="Enter Old Password" />
                      <i class="fa fa-check text_success"></i>
                      </div>

                      <div class="form_field">
                      <input type="password" name="new_pwd"  id="new_pwd" class="" placeholder="Enter New Password"  />
                      </div>
                      <div class="form_field">
                      <input type="password" name="cnfrm_new_pwd"  id="cnfrm_new_pwd" placeholder="Confirm New Password"/>
                      </div>
                      <div class="form_btn">
                      <input type="submit" name="update" class="" value="Update Password" onclick="return validate()" />
                      </div>
                      
                    </div>
         
                </form>
                <!--End new user Updation From-->
            <?php } ?>
        </div>
        </div>
        <!--start change image popup div-->
       <?php include'include/profile_popup.php';?>
       <!--End change image popup div-->  
    </div>
    <?php include('include/footer.php');?>
    <script src="js/ajaxCall.js"></script>
  </body>
</html>
<?php
require('./class/init.php');
	//Creating object of user entity
    $user    = Users::getInstance();
    //Getting redirect object
    $redirect=$init->getRedirect();
    //Getting redirect object
    $session=$init->getSession();
    //Starting Session
    $session->startSession();
    //check session  
    //echo '<pre>';print_r($_SESSION);die("aa");
    if($session->__get("username"))
    {
      $redirect->redirect("index.php");
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <?php include('include/inc.link.php');?>
        <!--start popup for change user profile-->
      
         <!--include data table file-->

      
<script src="js/jquery.validate.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

		$('.login').click(function (e) { 
			e.preventDefault();
			var data = {};
			data.username=$(".username").val();
			data.password=$(".password").val();
			
			$.ajax({
				type: "POST",
				url: "ajaxSignin.php",
				data: data,
				//cache: false,
				success: function (response) {
					//alert(response);
					if(response=="true"){
            <?php if($session->__get("editpray") &&  $session->__get("editpray") == "true"){?>
              window.location.replace("editpray.php?id=<?php echo $session->__get("editprayid")?>");
            <?php }elseif($session->__get("editComments") &&  $session->__get("editComments") == "true"){?>
              window.location.replace("editComments.php?commentId=<?php echo $session->__get("editCommentsId")?>&id=<?php echo $session->__get("prayId")?>&encrypt=<?php echo $session->__get("encrypt")?>");
              <?php }else{?>
             window.location.replace("index.php");
            <?}?>
           }
					else{
							alert("Please enter valid username and password")
					 }
				}
			}); 
		  return false;
	 });
  });
</script>

</head>

<body>
<div class="signin_page">
	<div class="login_register_logo">
    	<a href="index.php"><img src="images/logo_login.png" alt="" /></a>
    </div>
    <div class="form_box">
    	<h2>Already have an account!</h2>
        <div class="facebook_btn">
          <!--https://www.facebook.com/dialog/oauth?client_id=458146554335271&redirect_uri=http://gigaparse.com/clients/praysocially/fb/?fbTrue=true&scope=email,user_likes,publish_stream-->
        	<a href="https://www.facebook.com/dialog/oauth?client_id=<?php echo $config['App_ID'].'&redirect_uri='.$config['callback_url'].'&scope=email,user_likes,publish_stream';?>">
          <i class="fa fa-facebook"></i> Sign in with Facebook</a>
            <p>-- or --</p>
        </div>
        <form method="post" action="" id="signinfrm">
        <div class="form_inr">
        <div class="form_field">
       	  <input type="text" class="username" placeholder="Username" name="username" required/>
          </div>
          <div class="form_field">
          <input type="password" class="password" name="password" placeholder="Password" required/>
          </div>
          <div class="form_btn">
          <input type="submit" class="login" name="login" id="login" value="Sign in" />
          </div>
          <p>Forget Password? <a href="sendEmail.php">Click Now!</a></p></br>
           <p>Don't have an account? <a href="signup.php">Register Now!</a></p>
        </div>
        </form>
    </div>
    <?php include('include/footer.php');?>
</div>
</body>
</html>

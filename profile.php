<?php
ini_set('display_errors', 1);

require('./class/init.php'); 
 
    $users       = Users::getInstance();
    $redirect    = $init->getRedirect();
    $session     = $init->getSession();


    $session->startSession();

    /* check session  */

if(!$session->__get("username"))
{
    $redirect->redirect("index.php");
}
 
    $id          = $_SESSION['id'];
    $username    = $_SESSION['username']; 

    $limit       = 6;
 
    /* calling function for selecting allPray from praytb and countPray table for login user */

    $prays       = $users->allPrayLoginUser($id,$limit);
  
    /* calling function of changeImage of login user update */

    $updateImage = $users->updateprofile($id);
 
if($session->__get("limit"))
{
$session->__unset("limit");   
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include('include/inc.link.php');?>
        <!--start popup for change user profile-->
        
        <style>
        
            .user_pic a:hover {
            color:#fff;
            background:#000 !important;
            }
            .user_pic a {
            background:transparent !important;
            }
            .logo a {outline:none; text-decoration:none !important; border:none;}
        
        </style>
        <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="js/jquery.validate.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        
        <script type="text/javascript">
           $(document).ready(function() {
                
                //Hide all comments text box on page load
                $('.commentContent').hide();
                $('.saveCommments').hide();
                
                
            });
            
            //function for show selected comments box
            function myfunction(id)
            {
                var commentid="#commentContent"+id;
                var submitid="#saveCommments"+id;
                $(commentid).show();
                $(submitid).show();

            }

            //save Comments records in to database
            function saveCommentsData(id){

                //this comment id create for  gettting comment text box value  
                var commentid="commentContent"+id;

                var prayUserId=document.getElementById('prayUserId').value;
               
                var commentUserId=document.getElementById('commentUserId').value;
                
                var getPrayId=document.getElementById('getPrayId').value;

                var commentsText=document.getElementById(commentid).value;

                var myKeyVals = { prayUserId :prayUserId, commentUserId : commentUserId, getPrayId : getPrayId, commentsText : commentsText}

                


                $.ajax({
                    type: 'POST',
                    url: "ajaxComments.php",
                    data: myKeyVals,
                    dataType: "text",
                    success: function(resultData) {

                        if(resultData){

                            alert("comments post successfully") 
                            location.reload();
                        }
                        else{

                            alert("there are some error");
                        }
                    }
                });
            
            }
        </script>
    </head>

    <body> 
        <?php 
    include('include/header.php');
    include('include/top_banner_home.php');
    ?>

        
    <div class="container">
    <div class="row">
    <div class="">
        <div class="profile_box">
        <div class="timeline_big_pic">
        
            <img  src="images/timeline_pic.jpg" alt="" />
        </div>
        <div class="user_details">
            <div class="user_pic">  

                <a href="#link1" title="" class="link" data-toggle="modal">Change profile picture</a>
                <img class="changeimage" style="max-width:160px;" src="<?php if(!empty($updateImage['imageSrc']))echo $updateImage['imageSrc'];else echo "uploads/user_pic.jpg"; ?>"  alt="" />
            </div>
            <div class="user_disc">
            <div class="user_name">
                <?php if($_SESSION['username']){?>
                <p><a href="#"><?php echo $username; ?></a> <span>Priest</span></p>
                <?php } else{?>
                <p><a href="#"><?php echo "Guest"; ?></a> <span>Priest</span></p>
                <?php }?>
            </div>
            <div class="clear"></div>
            <div class="btn_profile">
                <a href="viewSupportedPray.php?id=<?php echo $id;?>" class="active">View supported prayers</a>
                <a href="viewMyPray.php?id=<?php echo $id; ?>" class="active">View my Prayers</a>
                <a href="unBlockPrayer.php" class="active">Unblock Prayers</a>
                <a href="blockUserStatus.php?id=<?php echo $id; ?>" class="active">View Block/Unblock User</a>
                <a href="myfollower.php?id=<?php echo $session->__get("id"); ?>" class="active">View My Followers</a>
            </div>
            </div>
        </div>
        <div class="clear"></div>
        </div>
        <div class="clear"></div>
        </div>
        </div>
        <div class="prayer_box">
         
            <ul id="prayLoadProfileId" class="prayLoadProfileClass">
                <!-- show allPray which is pray by login user-->
                <?php if(!empty($prays)){ foreach ($prays as $pray){ ?>
                <li>
                    
                        <div class="prayer_content">
                            <p><?php echo $pray['title']; ?></p>    
                            <a href="showPray.php?id=<?php echo $pray['id']; ?> ">Read more</a>
                        </div>
                        <div class="prayer_counter_box">
                                 <div class="praycount_left">
                                    <img src="images/hand.png" alt="" />
                                    <p><span class="single_pray" id="<?php echo $pray['id'];?>"><?php echo $count=$users->countPray($pray['id']);?> </span>Prayers</p>
                                </div>
                                <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                    <p class="creator_name"><?php $author=$users->author($pray['user_id']); ?>
                                       <a href ="author_profile.php?id=<?php echo $author[0]['id']; ?>" >

                                                <?php echo $author[0]['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <div class="prayer_btn">
                            <a class="Pray like_prayer" id="<?php echo $pray['id'] ?>"  href="javascript:void(0);" value="<?php echo $pray['id'] ?>">+1 Prayer</a>
                            <a href="javascript:void(0);" class="share_fb"><img src="images/fshare.jpg" alt="" /></a>
                            <div class="clear"></div>
                        </div>
                </li>
               <?php } }?>
            </ul>
            <div class="clear"></div>
            
            
            
            <div class="load_more">
                <a href="javascript:void(0);" class="loadMoreProfile"><i class="fa fa-refresh"></i> Load More</a>
            </div>
        </div>
        <!--start change image popup div-->
        <div>
            <div id="link1" class="modal fade bs-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                         <div class="modal-header ">
                            <!--start uploadImage Form-->
                            <form id="uploadImage" class="forminfo" action="" method="post" enctype="multipart/form-data">
                                <div id="selectImage">
                                    <label>Select Your Image</label><br/>
                                    <input type="file" name="file" id="file" required />
                                    <input type="hidden" name="user_id" value="<?php echo $id;?>"/>
                                    <input type="submit" name="uploadImage" value="Upload" class="primarybtn submit" />
                                </div>
                            </form>
                            <!--End uploadImage Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <!--End change image popup div-->  
    </div>
    <?php include('include/footer.php');?>
    <script src="js/ajaxCall.js"></script>
    </body>
</html>
<?php 
ini_set('display_errors', 1);
require('./class/init.php'); 

    $redirect           = $init->getRedirect();
    $users              = Users::getInstance();
    $comments           = Comments::getInstance();
    $session            = $init->getSession();
    $session->startSession();
    
    /* calling function of changeImage of login user update */
 
    $updateImage        = $users->updateprofile($session->__get("id"));
 
    /* check session */  
    
    if(!$session->__get("username"))
    {
        $redirect->redirect("index.php");
    }
    
    $username           = $_SESSION['username']; 
    
    /*
        Function : For select all comments block user for particular Prayer which create by login user
        Create   : Braj
        Date     : 30/07/2015

    */

    $selectBlockPrayer  = $comments->selectBlockPrayer($session->__get("id")); 
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php 
          include('include/inc.link.php');
        
        /* start popup for change user profile */ 
        
         include('include/inc.link-profile.php');
        ?>
    
        <style>
            .user_pic a:hover {
            color:#fff;
            background:#000 !important;
            }
            .user_pic a {
            background:transparent !important;
            }
            .logo a {outline:none; text-decoration:none !important; border:none;}
        </style>
    </head>
    <body> 
        <?php 
            include('include/header.php');
            include('include/top_banner_home.php');
            include('profile.newheader.php');
        ?>  
        <div class="container">
        <div class="row">
        <div class="">
            <div class="profile_box">
           
            <div class="clear"></div>
            </div>
            </div>
            <div class="">
            <div class="panel panel-default">
                <div class="panel-body">
                 <script>
                   jQuery(document).ready(function()
                    {
                        
                        $(".click_button").on("click", function(e){
                            var text = $(e.target).text();
                            if(text == 'unBlock'){
                                if (confirm("Are you sure you want to unBlock User")) {
                                        var msg = prompt("Please enter your Message");
                                        var txt = $.trim( msg );
                                        if(txt){
                                        unblockId = this.id;
                                        $.ajax({
                                            type: "POST",
                                            url: "ajaxCall.php",
                                            data: { unblockId: unblockId},
                                            success: function(data)
                                            {
                                                if(data == "true"){
                                                    $("#"+unblockId+"").parent().parent().remove();     
                                                    alert("Prayer unBlock successfully");
                                                }else{
                                                    alert('Some Error');
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                            
                        });
                    });
                </script>
                    <script type="text/javascript">
                    jQuery(document).ready(function($)
                    {
                        $("#example-1").dataTable({
                            aLengthMenu: [
                                [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
                            ]
                        });
                    });
                    </script>
                    <table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>User Name</th>
                                <th>Prayer</th>
                               <th>Status</th>
                            </tr>
                        </thead>
                    
                        <tfoot>
                            <tr>
                                <th>User Name</th>
                                <th>Prayer</th>
                                <th>Status</th>
                            </tr>
                        </tfoot>
                    
                        <tbody>
                           <?php if(!empty($selectBlockPrayer)){
                           foreach ($selectBlockPrayer as $key => $value){ ?>
                               
                           
                            <tr>
                                <td><?php $uname = $comments->selectUsrName($value['blockPrayForUserId']);foreach ($uname as $name){
                                    echo $name['username'];
                                }?>
                                </td>
                                <td><?php $prayName = $comments->selectPrayer($value['blockPrayId']);foreach ($prayName as $prayers => $pray) {
                                        echo $pray['title'];
                                   }?>
                                 </td>

                                <td>
                                    <a id="<?php echo $value['id'];?>" class="btn btn-success block click_button" values="<?php echo $value['id'];?>" data-toggle="modal" role="button" href="#">unBlock</a>
                                   
                                </td>
                            </tr>
                            <?php 
                            } } ?> 
                        </tbody>
                    </table>
                </div>
            </div>

            </div>
            <!--start change image popup div-->
           <?php include'include/profile_popup.php';?>
           <!--End change image popup div-->  
        </div>
        <?php include('include/footer.php');?>
        <script src="js/ajaxCall.js"></script>

        <!-- Imported styles on this page -->
        <link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">

        <!-- Bottom Scripts -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/TweenMax.min.js"></script>
        <script src="assets/js/resizeable.js"></script>
        <script src="assets/js/joinable.js"></script>
        <script src="assets/js/xenon-api.js"></script>
        <script src="assets/js/xenon-toggles.js"></script>
        <script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>


        <!-- Imported scripts on this page -->
        <script src="assets/js/datatables/dataTables.bootstrap.js"></script>
        <script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
        <script src="assets/js/datatables/tabletools/dataTables.tableTools.min.js"></script>


        <!-- JavaScripts initializations and stuff -->
        <script src="assets/js/xenon-custom.js"></script>


    </body>
</html>
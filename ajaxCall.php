<?php
require('./class/init.php');
    //object for session class
    $session=$init->getSession();
    //Starting Session
    $session->startSession();
    //create object for mail class
   

    $user = Users::getInstance();
    $admin = Admin::getInstance();
    $comments = Comments::getInstance();
    $mail = eMail::getInstance();
    if(isset($_SESSION['username'])){
    $username=$_SESSION['username'];
    //calling function for selecting userId from users table 
    $arrayId=$user->selectUserId($username);

    //calling function for selecting allPray from praytb and countPray table for login user
    $user_id=$arrayId['id'];
}
     if (isset($_POST['pray_id'])){
        $success=$user->addPray($_POST['pray_id'],$session->__get("id"));
        echo json_encode($success);
    }

    if (isset($_POST['follow_id'])){

        $success=$user->follow($_POST['follow_id'],$user_id);
        echo json_encode($success);
    }



    //remove user by admin

    if (isset($_POST['remove'])){
        $success=$admin->deleteUser($_POST['remove']);
        echo json_encode($success);
    }
     //block user by admin
    if (isset($_POST['block'])){
        $resultArray = explode("_",$_POST['block']);
        $id = $resultArray[1];
        $success=$admin->blockUser($id,$_POST['message']);
        
    }
     //unblock user by admin
    if (isset($_POST['unblock'])){
        $resultArray = explode("_",$_POST['unblock']);
        $id = $resultArray[1];
        $success = $admin->unblockUser($id);
        
    }
    //unblock Pray by admin
    if (isset($_POST['unblockPray'])){
        $resultArray = explode("_",$_POST['unblockPray']);
        $id = $resultArray[1];
        $success=$admin->updatePray($id,$status=0);
        
        
    }
    //block Pray by admin
    if (isset($_POST['blockPray'])){
        $resultArray  =   explode("_",$_POST['blockPray']);
        $id           =   $resultArray[1];
        $data         =   $user->selectData('Users','email',array('id' => $_POST['user_id']));
        $email        =   $data[0]['email'];
        $blockedby    =   $session->__get("id");
        $success = $admin->updatePray($id,$status=1,$blockedby);
        if($success){
            $mail->blockedPray($id,$_POST['message'],$email);
            echo "true";
        }else{
            echo "false";
        }
    }

     /*

     block CommentsUser by Login User basis on max number ofBlockComments
     Create :  Braj
     Date   : 28/07/2015

    */
    if (isset($_POST['blockUser']))
    {
        $array   = array('loginUserId' => $_POST['loginUserId'],'blockUser' => $_POST['blockUser'],'uEmail' => $_POST['uEmail'],'message' => $_POST['message']);
        $result  = $mail->userBlockByprayerAuthor($array);
        $success = $comments->blockUser($_POST['loginUserId'],$_POST['blockUser'],$_POST['message']);
        echo json_encode($success);
        //require('blockCommenUser.php');
    }

    /*

     Function : UnBlock Prayer for comments user
     Create   : Braj
     Date     : 31/07/2015

    */
    if (isset($_POST['unblockId']))
    { 
        $success=$comments->unBlockPrayUser($_POST['unblockId']);
        echo json_encode($success);
    }
    /*

     UnBlock CommentsUser by Login User
     Create :  Braj
     Date   : 28/07/2015

    */

    if (isset($_POST['UnblockUser']))
    {
        $resultData = $mail->userUnblockByprayerAuthor($_POST['uEmail']);
        $success    = $comments->UnblockUser($_POST['loginUserId'],$_POST['UnblockUser']);
        echo json_encode($success);
        //require('blockCommenUser.php');
    }
 /*

     UnBlock Comments 
     Create :  Braj
     Date   : 28/07/2015

    */

    if (isset($_POST['unblockComments'])){
        $success=$comments->UnBlockComments($_POST['unblockComments']);
    //require('blockCommenUser.php');
    }
// remove pray by admin
    if (isset($_POST['removePray'])){
        $success= $admin->deletePray($_POST['removePray']);
        echo json_encode($success);
    }

    //change iamge function calling for login user
    if (isset($_POST['src'])){
        $success=$user->changeImage($_POST['user_id'],$_POST['src']);
        echo json_encode($success);
    }
    if (isset($_POST['todayPray']))
    {
        $success=$user->todayPray();
        echo $success['COUNT(id)'];
    }

    //button call lode more ushing ajax for index.php page
    if (isset($_POST['loadMore']))
    {  
     
        if(!($session->__get("limit"))){ 
         $limit=6;

        $limit =$limit+$_POST['loadMore'];
        //set limit in session
        $session->__set("limit",$limit);
    }
    else
    { 
        $limit =$session->__get("limit")+$_POST['loadMore'];
        //set limit in session
        $session->__set("limit",$limit);
    }
        //calling function for all Pray 
        $prays=$user->showPray($session->__get("limit"));
        ?>
          <?php foreach($prays as $pray) { ?>
            <li class="abc">
                <div class="prayer_content">
                <p> <?php if(strlen($pray['title'])<30) echo $pray['title']; else { $title=substr($pray['title'],0,30); echo $title."....";}?></p>
                <a href="showPray.php?id=<?php echo $pray['id'] ?>">Read more</a>
                </div>
              
               <?php if($session->__get("username")) {?>

                        <div class="prayer_counter_box">
                                 <div class="praycount_left">
                                    <img src="images/hand.png" alt="" />
                                    <p><span class="single_pray" id="<?php echo $pray['id'];?>"><?php echo $count=$user->countPray($pray['id']);?> </span>Prayers</p>
                                </div>
                                <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                    <p class="creator_name"><?php $author=$user->author($pray['user_id']); ?>
                                       <a href ="author_profile.php?id=<?php echo $author[0]['id']; ?>" >

                                                <?php echo $author[0]['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <div class="prayer_btn">
                            <a class="Pray like_prayer" id="<?php echo $pray['id'] ?>"  href="javascript:void(0);" value="<?php echo $pray['id'] ?>">+1 Prayer</a>
                            <a href="javascript:void(0);" class="share_fb"><img src="images/fshare.jpg" alt="" /></a>
                            <div class="clear"></div>
                        </div>
                        <? } else{?>
                         <div class="prayer_counter_box">
                                 <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                    <p class="creator_name"><?php $author=$user->author($pray['user_id']); ?>
                                       <a href ="author_profile.php?id=<?php echo $author[0]['id']; ?>" >

                                                <?php echo $author[0]['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <? }?>
                
            </li>


        <?php } ?>
        <script src="js/ajaxCall.js"></script>
<?php
    }
    if (isset($_POST['loadMorecurlog']))
    {  
     
        if(!($session->__get("limit"))){ 
         $limit=6;

        $limit =$limit+$_POST['loadMorecurlog'];
        //set limit in session
        $session->__set("limit",$limit);
    }
    else
    { 
        $limit =$session->__get("limit")+$_POST['loadMorecurlog'];
        //set limit in session
        $session->__set("limit",$limit);
    }
        //calling function for all Pray 
        $prays=$user->viewCurLogPray($session->__get("limit"));
        ?>
          <?php if(!empty($prays)){
          foreach($prays as $pray) { ?>
            <li>
                <div class="prayer_content">
                <p> <?php if(strlen($pray['title'])<30) echo $pray['title']; else { $title=substr($pray['title'],0,30); echo $title."....";}?></p>
                <a href="showPray.php?id=<?php echo $pray['id'] ?>">Read more</a>
                </div>
              
                 <?php if($session->__get("username")) {?>

                        <div class="prayer_counter_box">
                                 <div class="praycount_left">
                                    <img src="images/hand.png" alt="" />
                                    <p><span class="single_pray" id="<?php echo $pray['id'];?>"><?php echo $count=$user->countPray($pray['id']);?> </span>Prayers</p>
                                </div>
                                <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                    <p class="creator_name">
                                       <a href ="author_profile.php?id=<?php echo  $pray['uid']; ?>" >

                                                <?php  echo $pray['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <div class="prayer_btn">
                            <a class="Pray like_prayer" id="<?php echo $pray['id'] ?>"  href="javascript:void(0);" value="<?php echo $pray['id'] ?>">+1 Prayer</a>
                            <a href="javascript:void(0);" class="share_fb"><img src="images/fshare.jpg" alt="" /></a>
                            <div class="clear"></div>
                        </div>
                        <? } else{?>
                         <div class="prayer_counter_box">
                                 <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                   <p class="creator_name">
                                       <a href ="author_profile.php?id=<?php echo  $pray['uid']; ?>" >

                                                <?php  echo $pray['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <? }?>
            </li>


        <?php } }?>
        <script src="js/ajaxCall.js"></script>
<?php
    }

    //button call load more ushing ajax for profile.php page
    if (isset($_POST['loadMoreProfile']))
    { 

        if(!($session->__get("limit"))){ 
         $limit=6;

        $limit =$limit+$_POST['loadMoreProfile'];
        //set limit in session
        $session->__set("limit",$limit);
        }
        else{  
            $limit =$session->__get("limit")+$_POST['loadMoreProfile'];
            //set limit in session
            $session->__set("limit",$limit);
        }
        //calling function for all Pray 
        $prays=$user->allPrayLoginUser($user_id,$session->__get("limit"));
        ?>
        <!--Here checking Iff $prays is Empty Or Not-->
          <?php /*Start IF*/if(!empty($prays)){ /*start foreach */foreach($prays as $pray) { ?>
            <li>
                <div class="prayer_content">
                <p> <?php if(strlen($pray['title'])<30) echo $pray['title']; else { $title=substr($pray['title'],0,30); echo $title."....";}?></p>
                <a href="showPray.php?id=<?php echo $pray['id'] ?>">Read more</a>
                </div>
                <?php if($session->__get("username")) {?>

                        <div class="prayer_counter_box">
                                 <div class="praycount_left">
                                    <img src="images/hand.png" alt="" />
                                    <p><span class="single_pray" id="<?php echo $pray['id'];?>"><?php echo $count=$user->countPray($pray['id']);?> </span>Prayers</p>
                                </div>
                                <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                    <p class="creator_name"><?php $author=$user->author($pray['user_id']); ?>
                                       <a href ="author_profile.php?id=<?php echo $author[0]['id']; ?>" >

                                                <?php echo $author[0]['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <div class="prayer_btn">
                            <a class="Pray like_prayer" id="<?php echo $pray['id'] ?>"  href="javascript:void(0);" value="<?php echo $pray['id'] ?>">+1 Prayer</a>
                            <a href="javascript:void(0);" class="share_fb"><img src="images/fshare.jpg" alt="" /></a>
                            <div class="clear"></div>
                        </div>
                        <? } else{?>
                         <div class="prayer_counter_box">
                                 <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                    <p class="creator_name"><?php $author=$user->author($pray['user_id']); ?>
                                       <a href ="author_profile.php?id=<?php echo $author[0]['id']; ?>" >

                                                <?php echo $author[0]['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <? }?>
                
            </li>
        <?php /*End Foreach*/} /*End If*/}else{exit();} ?>
        <script src="js/ajaxCall.js"></script>
<?php
    }

    
    //button call lode more ushing ajax for "view Supported Pray" button click On profile.php page
    if (isset($_POST['loadMoreSupportedPray']))
    { 

        if(!($session->__get("limit"))){ 
         $limit=6;

        $limit =$limit+$_POST['loadMoreSupportedPray'];
        //set limit in session
        $session->__set("limit",$limit);
        }
        else{  
            $limit =$session->__get("limit")+$_POST['loadMoreSupportedPray'];
            //set limit in session
            $session->__set("limit",$limit);
        }
        //calling function for all Pray 
        $prays=$user->viewSuportedPray($user_id,$session->__get("limit"));
        ?>
          <?php foreach($prays as $pray) { ?>
            <li>
                <div class="prayer_content">
                <p> <?php echo $pray['title'];?></p>
                <a href="showPray.php?id=<?php echo $pray['id'] ?>">Read more</a>
                </div>
                 <?php if($session->__get("username")) {?>

                        <div class="prayer_counter_box">
                                 <div class="praycount_left">
                                    <img src="images/hand.png" alt="" />
                                    <p><span class="single_pray" id="<?php echo $pray['id'];?>"><?php echo $count=$user->countPray($pray['id']);?> </span>Prayers</p>
                                </div>
                                <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                    <p class="creator_name"><?php $author=$user->author($pray['user_id']); ?>
                                       <a href ="author_profile.php?id=<?php echo $author[0]['id']; ?>" >

                                                <?php echo $author[0]['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <div class="prayer_btn">
                            <a class="Pray like_prayer" id="<?php echo $pray['id'] ?>"  href="javascript:void(0);" value="<?php echo $pray['id'] ?>">+1 Prayer</a>
                            <a href="javascript:void(0);" class="share_fb"><img src="images/fshare.jpg" alt="" /></a>
                            <div class="clear"></div>
                        </div>
                        <? } else{?>
                         <div class="prayer_counter_box">
                                 <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                    <p class="creator_name"><?php $author=$user->author($pray['user_id']); ?>
                                       <a href ="author_profile.php?id=<?php echo $author[0]['id']; ?>" >

                                                <?php echo $author[0]['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <? }?>
                
            </li>


        <?php } ?>
        <script src="js/ajaxCall.js"></script>
<?php
    }
    //
    
    //button call load more using ajax for "view Supported Pray" button click On profile.php page
    if (isset($_POST['loadMoreAuthor']))
    { 

        if(!($session->__get("limit"))){ 
         $limit=6;

        $limit =$limit+$_POST['loadMoreAuthor'];
        //set limit in session
        $session->__set("limit",$limit);
        }
        else{  
            $limit =$session->__get("limit")+$_POST['loadMoreAuthor'];
            //set limit in session
            $session->__set("limit",$limit);
        }
        //calling function for all Pray 
        $prays=$user->viewMyPray($session->__get("author_id"),$session->__get("limit"));
        ?>
          <?php foreach($prays as $pray) { ?>
            <li>
                <div class="prayer_content">
                <p> <?php if(strlen($pray['title'])<30) echo $pray['title']; else { $title=substr($pray['title'],0,30); echo $title."....";}?></p>
                <a href="showPray.php?id=<?php echo $pray['id'] ?>">Read more</a>
                </div>
                 <?php if($session->__get("username")) {?>

                        <div class="prayer_counter_box">
                                 <div class="praycount_left">
                                    <img src="images/hand.png" alt="" />
                                    <p><span class="single_pray" id="<?php echo $pray['id'];?>"><?php echo $count=$user->countPray($pray['id']);?> </span>Prayers</p>
                                </div>
                                <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                    <p class="creator_name"><?php $author=$user->author($pray['user_id']); ?>
                                       <a href ="author_profile.php?id=<?php echo $author[0]['id']; ?>" >

                                                <?php echo $author[0]['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <div class="prayer_btn">
                            <a class="Pray like_prayer" id="<?php echo $pray['id'] ?>"  href="javascript:void(0);" value="<?php echo $pray['id'] ?>">+1 Prayer</a>
                            <a href="javascript:void(0);" class="share_fb"><img src="images/fshare.jpg" alt="" /></a>
                            <div class="clear"></div>
                        </div>
                        <? } else{?>
                         <div class="prayer_counter_box">
                                 <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                    <p class="creator_name"><?php $author=$user->author($pray['user_id']); ?>
                                       <a href ="author_profile.php?id=<?php echo $author[0]['id']; ?>" >

                                                <?php echo $author[0]['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <? }?>
                
            </li>


        <?php } ?>
        <script src="js/ajaxCall.js"></script>
<?php
    }
    





    //button call lode more ushing ajax for "view My Pray" button click On profile.php page
    if (isset($_POST['loadMoreMyPray']))
    { 

        if(!($session->__get("limit"))){ 
         $limit=6;

        $limit =$limit+$_POST['loadMoreMyPray'];
        //set limit in session
        $session->__set("limit",$limit);
        }
        else{  
            $limit =$session->__get("limit")+$_POST['loadMoreMyPray'];
            //set limit in session
            $session->__set("limit",$limit);
        }
        //calling function for all Pray 
        $prays=$user->viewMyPray($user_id,$session->__get("limit"));
        ?>
          <?php if(!empty($prays)){foreach($prays as $pray) { ?>
            <li>
                <div class="prayer_content">
                <p> <?php if(strlen($pray['title'])<30) echo $pray['title']; else { $title=substr($pray['title'],0,30); echo $title."....";}?></p>
                <a href="showPray.php?id=<?php echo $pray['id'] ?>">Read more</a>
                </div>
                 <?php if($session->__get("username")) {?>

                        <div class="prayer_counter_box">
                                 <div class="praycount_left">
                                    <img src="images/hand.png" alt="" />
                                    <p><span class="single_pray" id="<?php echo $pray['id'];?>"><?php echo $count=$user->countPray($pray['id']);?> </span>Prayers</p>
                                </div>
                                <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                    <p class="creator_name"><?php $author=$user->author($pray['user_id']); ?>
                                       <a href ="author_profile.php?id=<?php echo $author[0]['id']; ?>" >

                                                <?php echo $author[0]['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <div class="prayer_btn">
                            <a class="Pray like_prayer" id="<?php echo $pray['id'] ?>"  href="javascript:void(0);" value="<?php echo $pray['id'] ?>">+1 Prayer</a>
                            <a href="javascript:void(0);" class="share_fb"><img src="images/fshare.jpg" alt="" /></a>
                            <div class="clear"></div>
                        </div>
                        <? } else{?>
                         <div class="prayer_counter_box">
                                 <div class="creator_name_right">
                                    <p>Created By~</p>
                                    <br>
                                    <p class="creator_name"><?php $author=$user->author($pray['user_id']); ?>
                                       <a href ="author_profile.php?id=<?php echo $author[0]['id']; ?>" >

                                                <?php echo $author[0]['username'];  ?>
                                        </a>
                                    </p>
                                </div>
                        </div>
                        <? }?>
                
            </li>


        <?php }}else{exit();} ?>
        <script src="js/ajaxCall.js"></script>
<?php
    }
    
?>

<?php
class Mail {
    private static $instance=NULL;
    
    public static function getInstance(){ 
        if( self::$instance === NULL ) { 
            self::$instance = new self();
        }
        return self::$instance;
    }
    public static function unblockMail($email,$title) {
    	$message  =   "This Pray updated by user <br> Title :".$title." <br>";
  		$to       =   $email;
  		$subject  =   "Block Pray Responce";
  		$from     =   'Praysocially';
  		$headers  =   "From: " . strip_tags($from) . "\r\n";
  		$headers .=   "Reply-To: ". strip_tags($from) . "\r\n";
  		$headers .=   "MIME-Version: 1.0\r\n";
  		$headers .=   "Content-Type: text/html; charset=ISO-8859-1\r\n";
  		if (!mail($to,$subject,$message,$headers)) { 
  		print_r(error_get_last());
  		}
  		return true;
	}

	 //Function For the Blocked of prayer for admin
    public function blockedPray($pray_id, $msg = '', $email = ''){
    	//echo $email;die("kkk");
      $message  =   "Your prayer is block due to following reason: <br>";
      $message .=   $msg;
      $message .=   "<br>You can edit your pray from below link so it will be unlock again : <br>";
      $message .=   BASE_URL."/editpray.php?action=editpray&id=".$pray_id;
      $to       =   $email;
      $subject  =   "Block Pray";
      $from     =   'Praysocially';
      $headers  =   "From: " . strip_tags($from) . "\r\n";
      $headers .=   "Reply-To: ". strip_tags($from) . "\r\n";
      $headers .=   "MIME-Version: 1.0\r\n";
      $headers .=   "Content-Type: text/html; charset=ISO-8859-1\r\n";
      if (!mail($to,$subject,$message,$headers)) { 
        print_r(error_get_last());
      } 
    }
    /*
      Function  : Function For the Blocked Comment User for Particular Prayer By Pray Creater 
      Create    : Braj
      Date      : 04/08/2015

    */

    public function blockedCommentUser($pray_id, $msg = '', $email = ''){
      
      $message  =   "You are block due to following reason:  <br>";
      $message .=   $msg;
      $message .=   "<br>You are block for unwanted comments on this prayer  : <br>";
      //$message .=   BASE_URL."/editpray.php?action=editpray&id=".$pray_id;
      $to       =   $email;
      $subject  =   "Block Pray";
      $from     =   'Praysocially';
      $headers  =   "From: " . strip_tags($from) . "\r\n";
      $headers .=   "Reply-To: ". strip_tags($from) . "\r\n";
      $headers .=   "MIME-Version: 1.0\r\n";
      $headers .=   "Content-Type: text/html; charset=ISO-8859-1\r\n";
      if (!mail($to,$subject,$message,$headers)) { 
        print_r(error_get_last());
      } 
    }
}




<?php
class Users extends Init{
   protected $userName;
   protected $passWord;
   protected $id;
   protected static $tableName="Users";
  
   private static $instance=NULL;   
   public static function getInstance(){ 
     if( self::$instance === NULL ) { 
         self::$instance = new self();
     }
      return self::$instance;
   }
   //Setters
   public function setUsername ($userName){
      $this->userName = $userName;  
   }
  
   public function setPassword ($passWord){
      $this->passWord = $passWord;  
   }
   
  public function setId ($id){
      $this->id = $id;  
   }


   //Getters
   public function getUsername ($userName){
      return $this->userName;  
   }

   public function getPassword ($passWord){
      return $this->passWord;  
   }
  
   public function getId ($id){
      return $this->$id;  
   }
   

   //Functions
   public function getAllUsers(){

   }

   //Function for login user 
   public function login($array){
      $user=$this->_dbh->query("select * from Users where 
         username='".$array['username']."' and
         password='".md5($array['password'])."'
         ")->fetch(PDO::FETCH_ASSOC);
      if($user)
         return $user;
      else
         return false;
   }
   
   //Function for registration user
   public function registration($array){
    //serch user is already exsist (this search query for facebook login)
    $user=$this->_dbh->query("select * from Users where email='".$array['email']."'")->fetch(PDO::FETCH_ASSOC);
    if($user){
      return true;
      }else if(!$user){
      $this->_dbh->exec("insert into Users(username,password,email) values('".$array['username']."','".md5($array['password'])."','".$array['email']."')");
      return true;
      }
      else{
         return false;
       }
  }
     //Function Check register username do not exist in database 
   public function registerCheck($array){

      $user=$this->_dbh->query("select * from Users  where username='".$array['username']."' ")->fetchAll(PDO::FETCH_ASSOC);
      if($user)
         return true;
      else
         return false;
   }

   //Function for Insert Pray 
   public function pray($array){
       $user=$this->_dbh->exec("insert into praytb(title,pray,user_id) values('".$array['title']."','".$array['pray']."','".$array['user_id']."')");
        if($user)
           return true;
         else
            return false;
      }
      //function call for show all pray    
      function showPray($getLimit) {
      
      $limit=$getLimit;

       return $this->_dbh->query("SELECT * FROM Users,praytb WHERE praytb.user_id=Users.id and Users.block=0 and praytb.status=0 order by Users.id DESC LIMIT $getLimit")->fetchAll(PDO::FETCH_ASSOC);
      }
      
      //function call for show  pray By Id    
      function showPrayByid($id){
       return $this->_dbh->query("select * from praytb where id='$id'")->fetch(PDO::FETCH_ASSOC);
      }
 public function countPray($prayId){
    $value=$this->_dbh->query("select pray_id from countPray where pray_id='$prayId'")->fetchAll(PDO::FETCH_ASSOC);
    if($value)
    {
      return count($value);
    } else {
      return 0;
    }
  }
    //Function for Insert Pray 
    public function addPray($prayId,$uerId){
    $value=$this->_dbh->query("select * from countPray where pray_id='$prayId' and user_id='$uerId'")->fetch(PDO::FETCH_ASSOC);
    if($value)
    {
      $success=0;
      $this->_dbh->query("delete from countPray where id='".$value['id']."'");
    }else{
    $success=$this->_dbh->exec("insert into countPray(pray_id,user_id,date) values('".$prayId."','".$uerId."','".date('d-M-Y')."')");
    }
    if($success==1)
       return true;
     else
        return false;
    }


    //show today  pray
    public function todayPray(){
      return $this->_dbh->query("select COUNT(id) from countPray where date='".date('d-M-Y')."'")->fetch(PDO::FETCH_ASSOC);
    }
    //show today  Single  pray
    public function singlePray($pray_id){
      $value=$this->_dbh->query("select * from countPray where pray_id='$pray_id 'and date='".date('d-M-Y')."'")->fetchAll(PDO::FETCH_ASSOC);
      if ($value)
      {
      return count($value);
      } else {
        return 0;
      }
    }
  //select login user_ID from User table where username
  public function selectUserId($username){
    $userId=$this->_dbh->query("select id from Users where username='$username'")->fetch(PDO::FETCH_ASSOC);
    if($userId){
     
     return $userId;
    }
    else{
     
     return false;
    }
  }  
  //Show all pray for Login User
  public function allPrayLoginUser($id,$limit){ 
    //$allPray=$this->_dbh->query("select * from countPray where user_id='$id'")->fetchAll(PDO::FETCH_ASSOC)or die(mysql_error());
    $allPray=$this->_dbh->query("select p.* from praytb as p inner join countPray as c ON p.id=c.pray_id where c.user_id='$id' ORDER BY p.id desc LIMIT $limit")->fetchAll(PDO::FETCH_ASSOC);
     if($allPray){
          
          return $allPray;
      }
      else{

          return false;
      }
  }
  //changeImage of login user with 'imagePath', 'imageName','login _ userId';
  public function uploadUserImage($imgepath,$imagename,$user_id){ 

        $selectImage=$this->_dbh->query("select * from profiletb where user_id='".$user_id."'")->fetchAll(PDO::FETCH_ASSOC);
        
          if(!empty($selectImage)){
          
            $updateResult=$this->_dbh->exec("update profiletb set imageName='".$imagename."' , imageSrc='".$imgepath."' where user_id='".$user_id."' ");
              return $updateResult;
            }
            else{
              
                $insertResult=$this->_dbh->exec("insert into profiletb(user_id,imageName,imageSrc) values('".$user_id."','".$imagename."','".$imgepath."')");
              
              return $insertResult;
          }//End  if statement
  }//End function calling

  //calling function of update image of login user
  public function updateprofile($user_id){ 

      $updateImage=$this->_dbh->query("select * from profiletb where user_id='".$user_id."'")->fetch(PDO::FETCH_ASSOC);  
         
      if(isset($updateImage)){
          
             return $updateImage;
      }
      else{
     $id=0;
            $defaultImage=$this->_dbh->query("select * from profiletb where user_id='".$id."'")->fetch(PDO::FETCH_ASSOC);
            return false;
      }
  }

  //calling function of view supported prayers for profile.php page
  public function viewSuportedPray($id,$limit){ 

      $allPray=$this->_dbh->query("select p.* from praytb as p inner join countPray as c ON p.id=c.pray_id where c.user_id='$id' ORDER BY p.id DESC LIMIT $limit")->fetchAll(PDO::FETCH_ASSOC);
     if($allPray){
          
          return $allPray;
      }
      else{

          return false;
      }
  }
  public function viewMyPray($id,$limit){

      $result=$this->_dbh->query("select * from praytb where user_id='".$id."' ORDER BY id desc LIMIT $limit")->fetchAll(PDO::FETCH_ASSOC);
      if($result){
        return $result;
      }
      else{

        return false;
      }


  }
  


  //User detail by Id
  public function userdetail($id){
    $result=$this->_dbh->query("select * from Users where id='".$id."' ")->fetchAll(PDO::FETCH_ASSOC);
      if($result){
        return $result;
      }
      else{

        return false;
      }

  }

  //Function for update data
  public function update($table,$column,$where){
     //initialize variables

      $set="";
      $size=sizeof($column);
      $i=1;
      $whereSql="";
      $sizeWhere=sizeof($where);
      
      // extract column name and values of update data
      foreach ($column as $key => $value) {

                    if($i<$size)
                        $set.=" $key='$value' , ";
                    else
                        $set.=" $key='$value'";

                     $i++;
        }

      $i=1;
      
     // extract the values of where clause
    foreach ($where as $whereKey => $whereValue) {

                if($i<$sizeWhere)
                  $whereSql.=" $whereKey='$whereValue' and ";
                else
                  $whereSql.=" $whereKey='$whereValue'";

                  $i++;
      }
      
      $updateResult=$this->_dbh->exec("update $table set $set where $whereSql ");

       if($updateResult){
        return true;
       }else{
        return false;
       }
  }

  public function selectData($table,$column,$where){
     //initialize variables

     
      $i=1;
      $whereSql="";
      $sizeWhere=sizeof($where);
      
      if(!empty($where)){    
        // extract the values of where clause
        foreach ($where as $whereKey => $whereValue) {

                if($i<$sizeWhere)
                  $whereSql.=" $whereKey='$whereValue' and ";
                else
                  $whereSql.=" $whereKey='$whereValue'";

                  $i++;
        }
        $Result=$this->_dbh->query("select $column from $table where $whereSql")->fetchAll(PDO::FETCH_ASSOC);
      }
      else
      {
      
      $Result=$this->_dbh->query("select $column from $table")->fetchAll(PDO::FETCH_ASSOC);
      }
      
     

       
        return $Result;
      
  }

  //function Author Name
  public function author($author){
    $result=$this->_dbh->query("select * from Users where id='$author'")->fetchAll(PDO::FETCH_ASSOC);
    if($result){
        return $result;
      }
      else{

        return false;
      }

  }
  
  /*
  Function reset password on basis of encrypt key 
  Create By : Braj
  Date      : 21/07/2015
  
  */
  public function resetPasswordLink($array){
    
    $encrypt=$array['encrypt'];

    $password=$array['Password'];
    
    //Check If encrypt key is Empty Or Not for #Authorization 
    if (!empty($encrypt)){
      
    
      $query=$this->_dbh->exec("update Users set password='".md5($password)."',forgetPassword='' where forgetPassword='".$encrypt."'");   
     
       if(!empty($query)) {
           ?><script>alert("Your password changed sucessfully");window.location.assign('signin.php');</script><?php  
          return $query;
        }
        else{
              ?><script>alert("Your link was Destroyed...!");window.location.assign('signin.php');</script><?php  
      }
    }
    else{
              ?><script>alert("Your are not Authorized ...!");window.location.assign('signin.php');</script><?php  
    } 
  }
   public function countUserPray($user_id){
    $value=$this->_dbh->query("select * from praytb where user_id='$user_id '")->fetchAll(PDO::FETCH_ASSOC);
      if ($value)
      {
      return count($value);
      } else {
        return 0;
      }
  }
  public function follow($follow_id,$userid){
      $value=$this->_dbh->query("select * from followtb where follow_id='$follow_id' and user_id='$userid'")->fetch(PDO::FETCH_ASSOC);
    //echo '<pre>';print_r($value);die('d');
    if($value)
    {
    $this->_dbh->query("delete from followtb where fid='".$value['fid']."'");
    $success=0;
    }else{
    $success=$this->_dbh->exec("insert into followtb(follow_id,user_id) values('".$follow_id."','".$userid."')");
     }
     if($success==1)
       return true;
     else
        return false;
        

    }

//Script for information about follow table

   public function follow_info($follow_id,$userid){
    $value=$this->_dbh->query("select * from followtb where follow_id='$follow_id' and user_id='$userid'")->fetch(PDO::FETCH_ASSOC);
    //echo '<pre>';print_r($value);die('d');
    if($value)
    {
      return true;
    }
    else{
      return false;
    }
   }

   //Script For Follower's list Of User

   public function Follower($userid){
    $value=$this->_dbh->query("select * from followtb where follow_id='$userid'")->fetchAll(PDO::FETCH_ASSOC);
    //echo '<pre>';print_r($value);die('d');
    if($value)
    {
      return $value;
    }
    else{
      return false;
    }
   }
   public function viewCurLogPray($limit){
        $result=$this->_dbh->query("select * from (SELECT Users.username,Users.id as uid,praytb.* FROM Users left join praytb on praytb.user_id=Users.id WHERE Users.loginstatus='1' and Users.block='0' and praytb.status='0') as lg left join followtb as ft on lg.user_id=ft.follow_id LIMIT $limit ")->fetchAll(PDO::FETCH_ASSOC);
         if($result){
        return $result;
      }
      else{

        return false;
      }
    }
    /*

    */
    public function checkPassowrd($array){

        $result=$this->_dbh->query("select * from Users where id='".$array['id']."' and password='".md5($array['password'])."' ")->fetchAll(PDO::FETCH_ASSOC);
         
         if($result){
        return "success";
      }
      else{

        return "failled";
      }
    }
    /*

    */
    public function updateChangePassword($array){ 

        $query=$this->_dbh->exec("update Users set password='".$array['password']."' where id='".$array['id']."'");   
      
         if($query){
        return "success";
      }
      else{

        return "failled";
      }
    }
}//End of class definition
?>
<?php
class eMail extends Init{
    private static $instance=NULL;
    
    public static function getInstance(){ 
        if( self::$instance === NULL ) { 
            self::$instance = new self();
        }
        return self::$instance;
    }
    public static function unblockMail($email,$title) {
    $message  =   "This Pray updated by user <br> Title :".$title." <br>";
		$to       =   $email;
		$subject  =   "Block Pray Responce";
		$from     =   'Praysocially';
		$headers  =   "From: " . strip_tags($from) . "\r\n";
		$headers .=   "Reply-To: ". strip_tags($from) . "\r\n";
		$headers .=   "MIME-Version: 1.0\r\n";
		$headers .=   "Content-Type: text/html; charset=ISO-8859-1\r\n";
		if (!mail($to,$subject,$message,$headers)) { 
		print_r(error_get_last());
		}
		return true;
	}

	 //Function For the Blocked of prayer for admin
    public function blockedPray($pray_id, $msg = '', $email = ''){
    	//echo $email;die("kkk");
      $message  =   "Your prayer is block due to following reason: <br>";
      $message .=   $msg;
      $message .=   "<br>You can edit your pray from below link so it will be unlock again : <br>";
      $message .=   BASE_URL."/editpray.php?action=editpray&id=".$pray_id;
      $to       =   $email;
      $subject  =   "Block Pray";
      $from     =   'Praysocially';
      $headers  =   "From: " . strip_tags($from) . "\r\n";
      $headers .=   "Reply-To: ". strip_tags($from) . "\r\n";
      $headers .=   "MIME-Version: 1.0\r\n";
      $headers .=   "Content-Type: text/html; charset=ISO-8859-1\r\n";
      if (!mail($to,$subject,$message,$headers)) { 
      print_r(error_get_last());
      }else{
        //echo 'mail is send';

      } 
    }
     /*
      Function  : Function For the Blocked Comment User for Particular Prayer By Pray Creater 
      Create    : Braj
      Date      : 04/08/2015

    */

    public function blockedCommentUser($array){
      $email     =   $array['email'];
      $prayTitel =   $array['prayTitle'];
      $msg       =   '';

      $message   =   "You are block  due to following reason:  <br>";
      $message  .=   $msg;
      $message  .=   "<br>You are block for this prayer :".$prayTitel;
      $to        =   $email;
      $subject   =   "Block Comments User";
      $from      =   'Praysocially';
      $headers   =   "From: " . strip_tags($from) . "\r\n";
      $headers  .=   "Reply-To: ". strip_tags($from) . "\r\n";
      $headers  .=   "MIME-Version: 1.0\r\n";
      $headers  .=   "Content-Type: text/html; charset=ISO-8859-1\r\n";
      if (!mail($to,$subject,$message,$headers)) { 
        print_r(error_get_last());
      } 
    }
    /*
      Function  : Function For the Block Comment for Particular Prayer By Pray Creater 
      Create    : Braj
      Date      : 04/08/2015

    */

    public function blockedCommentsEmail($array){
      
      /*generate encrypt key for password reset link*/

      $encrypt      =   rand(150,date('dd/mm/yyyy'));
      $updateResult =   $this->_dbh->exec("update comments set encrypt='".$encrypt."' where id='".$array['commentsId']."' ");
      $email        =   $array['email'];
      $id           =   $array['PrayId'];
      $commentId    =   $array['commentsId'];

      $msg          =   '';
      $message      =   "Your comment is block due to following reason:  <br>";
      $message     .=   $msg;
      $message     .=   "<br>Your comments is block for unwanted contents , <br>";
      $message     .=   "<br>Edit your comments use given link : <br>";
      $message     .=   BASE_URL."/editComments.php?action=editComments&id=".$id."&commentId=".$commentId."&encrypt=".$encrypt;
      $to           =   $email;
      $subject      =   "Block Comments";
      $from         =   'Praysocially';
      $headers      =   "From: " . strip_tags($from) . "\r\n";
      $headers     .=   "Reply-To: ". strip_tags($from) . "\r\n";
      $headers     .=   "MIME-Version: 1.0\r\n";
      $headers     .=   "Content-Type: text/html; charset=ISO-8859-1\r\n";
      if (!mail($to,$subject,$message,$headers)) { 
        print_r(error_get_last());
      } 
    }
     /*

  Function  : For forget password link send to user given mailID 
  Create By : Braj
  Date      : 05/08/2015
  */
  public function sendMailToUser($email){ 
   
    $query=$this->_dbh->query("SELECT id FROM Users where email='".$email."'")->fetchAll(PDO::FETCH_ASSOC);
    
        
      if(count($query)>=1)
      { 
        foreach ($query as $Results) {
          
          /*generate encrypt key for password reset link*/

          $encrypt      = rand(150,date('dd/mm/yyyy'));

          $updateResult = $this->_dbh->exec("update Users set forgetPassword='".$encrypt."' where email='".$email."' ");
          $message      = "Your password reset link send to your e-mail address.";
          $to           = $email;
          $subject      = "Forget Password";
          $from         = 'Praysocially';
          $body         = 'Hi, <br/> <br/>Your Password Reset Link is  <br><br>';
          $body        .= BASE_URL."/resetPassword.php?encrypt=".$encrypt." <br>Solve your problems.";
          $headers      = "From: " . strip_tags($from) . "\r\n";
          $headers     .= "Reply-To: ". strip_tags($from) . "\r\n";
          $headers     .= "MIME-Version: 1.0\r\n";
          $headers     .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

          if (!mail($to,$subject,$body,$headers)) { 
             print_r(error_get_last());
          }
          else { ?><script style="color:#d96922; font-weight:bold; height:0px; margin-top:1px;">alert("Password reset link send to your mail ID!!");</script>
         <?php
          }     
        }
      }
      else
      {
          ?><script style="color:#d96922; font-weight:bold; height:0px; margin-top:1px;">alert("Account not found please signup now!!");</script>
         <?php
          //$message = "Account not found please signup now!!";
      }
  }

  /*

    Function  : For unBlock Comments User to inform you are UnBlock By prayer creater 
    Create By : Braj
    Date      : 05/08/2015
  
  */
  public function unblockCommentUserOnPrayer($array){ die("unblock comments user");




  }
  /* 
    Function : For send confirmation email to pray creater , comments update by comment user
    Create   : Braj
    Date     : 05/08/2015

  */
  public function confirmationMail($array){ 

    $email     =   $array['email'];
    $comment   =   $array['comments'];
    $msg       =   '';
    $message   =   "Please unblock comments user due to following reason:  <br>";
    $message  .=   $msg;
    $message  .=   "<br>Comments user update their comment :".$comment;
    $to        =   $email;
    $subject   =   "unBlock Comments User";
    $from      =   'Praysocially';
    $headers   =   "From: " . strip_tags($from) . "\r\n";
    $headers  .=   "Reply-To: ". strip_tags($from) . "\r\n";
    $headers  .=   "MIME-Version: 1.0\r\n";
    $headers  .=   "Content-Type: text/html; charset=ISO-8859-1\r\n";
    if (!mail($to,$subject,$message,$headers)) { 
      print_r(error_get_last());
    }else{

      return "success";
    } 
  }
  /*
    Function : For Send Email parmanent Block Comment user He didn,t see any prayer of creater 
    Create   : Braj
    Date     : 10/08/2015
  */
  public function userBlockByprayerAuthor($array){ 

    $email     =   $array['uEmail'];
    $msg       =   $array['message'];
    $message   =   "You are block  due to following reason:  <br>";
    $message  .=   $msg;
    $message  .=   "<br>You are block parmanent ";
    $to        =   $email;
    $subject   =   "Block parmanent";
    $from      =   'Praysocially';
    $headers   =   "From: " . strip_tags($from) . "\r\n";
    $headers  .=   "Reply-To: ". strip_tags($from) . "\r\n";
    $headers  .=   "MIME-Version: 1.0\r\n";
    $headers  .=   "Content-Type: text/html; charset=ISO-8859-1\r\n";
    if (!mail($to,$subject,$message,$headers)) { 
      print_r(error_get_last());
    }else{

      return "success";
    } 
  }
  /*
    Function : For Send Email UnBlock Comment user He can see any prayer of creater who unblock 
    Create   : Braj
    Date     : 10/08/2015
  */
  public function userUnblockByprayerAuthor($email){ 

    $message   =   "You are unBlock by prayer Author <br>";
    $message  .=   '</br>';
    $message  .=   "<br>please keep help other , God Bless you ! ";
    $to        =   $email;
    $subject   =   "UnBlock parmanent";
    $from      =   'Praysocially';
    $headers   =   "From: " . strip_tags($from) . "\r\n";
    $headers  .=   "Reply-To: ". strip_tags($from) . "\r\n";
    $headers  .=   "MIME-Version: 1.0\r\n";
    $headers  .=   "Content-Type: text/html; charset=ISO-8859-1\r\n";
    if (!mail($to,$subject,$message,$headers)) { 
      print_r(error_get_last());
    }else{

      return "success";
    } 
  }   

}




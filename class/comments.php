<?php
/*

  Create Comments Class for user can create comments on pray  
  Create By : Braj
  Date      : 24/07/2015

*/
class Comments extends Init{
   protected $userName;
   protected $passWord;
   protected $id;
   protected static $tableName="Users";
  
   private static $instance=NULL;   
   public static function getInstance(){ 
     if( self::$instance === NULL ) { 
         self::$instance = new self();
     }
      return self::$instance;
   }
   //Setters
   public function setUsername ($userName){
      $this->userName = $userName;  
   }
  
   public function setPassword ($passWord){
      $this->passWord = $passWord;  
   }
   
	public function setId ($id){
      $this->id = $id;  
   }


   //Getters
   public function getUsername ($userName){
      return $this->userName;  
   }

   public function getPassword ($passWord){
      return $this->passWord;  
   }
  
   public function getId ($id){
      return $this->$id;  
   }
   

   //Functions
   public function getAllUsers(){

   }

   //Function for login user comments on pray
  public function commentsOnPray($array){

      $insertResult=$this->_dbh->exec("insert into comments(prayId,prayUserId,commentsUserId,comments) VALUES('".$array['getPrayId']."','".$array['prayUserId']."','".$array['commentUserId']."','".$array['commentContent']."')");
      if ($insertResult) {
                return true;
              }
              else{

                  return false;

              }        


  } 
  //show comment on pray basis of prayID 
  public function showCommentsByPrayId($prayId){

     $query=$this->_dbh->query("select * from comments where prayId='$prayId' and blockComments='0' order by id desc")->fetchAll(PDO::FETCH_ASSOC);
      
      if(count($query)>=1){

        return $query;
      }
      else{
        return "failed";

      }
  }
  public function selectCommentsUserNameById($userId){


    $query=$this->_dbh->query("select * from Users where id='$userId' order by id desc")->fetchAll(PDO::FETCH_ASSOC);
      if(count($query)>=1){

        return $query;
      }
      else{
        return "failed";

      }
    
  }
  /*
   Function for block/unblock comments   
   Create : Braj
   Date   : 27/07/2015

  */
  public function blockComments($commentsID){
   
     $updateResult=$this->_dbh->exec("update comments set blockComments='1' where id='".$commentsID."' ");
   
      if (!empty($updateResult)) {
                return true;
      }
      else{

          return false;
      }
  } 
/*
   Function for show all block_comments_user_records from  comments table  
   Create : Braj
   Date   : 27/07/2015

  */
  public function selectBlockCommentsUser($loginUserId){

    $query=$this->_dbh->query("select COUNT(*) as comment,commentsUserId,blockUserStatus from comments where prayUserId='$loginUserId' and blockComments='1' group by commentsUserId order by id desc ")->fetchAll(PDO::FETCH_ASSOC);
     
      return $query;
  }
  public function selectCommentsUserName($commentsUserId){

    $query=$this->_dbh->query("select * from Users where id='$commentsUserId' order by id desc")->fetchAll(PDO::FETCH_ASSOC);
      
    if(count($query)>=1){

      return $query;
    }
    else{
      return false;
    }
  }
  public function blockUser($blockByUserId,$blockUserId,$message){

   
   $updateResult=$this->_dbh->exec("update comments set blockUserStatus='1',message='$message' where prayUserId='".$blockByUserId."'  and  commentsUserId='".$blockUserId."' ");
   if($updateResult){
      return "success";
    }
    else{

      return "failed";

    }
  }
public function UnblockUser($UnblockByUserId,$UnblockUserId){

   $updateResult=$this->_dbh->exec("update comments set blockUserStatus='0',message='' where prayUserId='".$UnblockByUserId."'  and  commentsUserId='".$UnblockUserId."' ");

   if($updateResult){
      return "success";
    }
    else{

      return "failed";

    }
  }
  public function singleshowUsers($loginUserId,$BlockUserId){
   
    $user=$this->_dbh->query("select COUNT(*) as comment,commentsUserId,blockUserStatus from comments where prayUserId='".$loginUserId."' and commentsUserId='".$BlockUserId."' and blockComments='1' group by commentsUserId order by id desc ")->fetchAll(PDO::FETCH_ASSOC);
   
    if($user)
       return $user;
    else
       return false;
  }
  public function selectCommentsData($loginUserId,$BlockUserId){
   
    $user=$this->_dbh->query("select * from comments where prayUserId='".$loginUserId."' and commentsUserId='".$BlockUserId."' and blockComments='1' order by id desc ")->fetchAll(PDO::FETCH_ASSOC);

    if($user)
       return $user;
    else
       return false;
  }
  public function selectPrayTitle($prayId){

    $user=$this->_dbh->query("select * from praytb where id='".$prayId."' ")->fetchAll(PDO::FETCH_ASSOC);

    if($user)
       return $user;
    else
       return false;
  }
  public function UnBlockComments($commentsId){

    $updateResult=$this->_dbh->exec("update comments set blockComments='0' where id='".$commentsId."' ");

    if($updateResult){
      return "success";
    }
    else{

      return "failed";

    }
  }
  public function showCommentsusers($id){

    $user=$this->_dbh->query("select * from comments where commentsUserId='".$id."' and blockUserStatus='1' ")->fetchAll(PDO::FETCH_ASSOC);
  
    if($user)
       return $user;
    else
       return false;



  }
  /*
    Function for block_prayer  
    Create : Braj
    Date   : 29/07/2015

  */
  public function blockPrayer($array){
   
    $id = $array['PrayId'];

    $insertResult = $this->_dbh->exec("insert into prayerBlock(blockPrayId,createPrayUserId,blockPrayForUserId,blockPrayStatus) VALUES('".$array['PrayId']."','".$array['loginUserId']."','".$array['commentsUserId']."','1')");
    
    $updateResult = $this->_dbh->exec("update comments set blockComments='1' where prayId='".$array['PrayId']."' and commentsUserId='".$array['commentsUserId']."' ");  
      
      if ($updateResult) {?><script>alert("Comments user successfully block for that particular Prayer !");</script><?php
               
      }
      else{?></script>alert("Error");</script><?php
              
      }      
  }
  /*
    Function for select  block_prayer on basis login user id 
    Create : Braj
    Date   : 30/07/2015

  */
  public function selectBlockPray($userId,$prayId){

    $user = $this->_dbh->query("select * from prayerBlock where blockPrayForUserId='".$userId."' and blockPrayId='".$prayId."' ")->fetchAll(PDO::FETCH_ASSOC);
      if($user)
         return $user;
      else
         return false;
  }
  /*
    Function : For select  comments detail for Edit basis on comments ID 
    Create   : Braj
    Date     : 30/07/2015

  */
  public function selectComments($id){

    $user = $this->_dbh->query("select * from comments where id='".$id."' ")->fetchAll(PDO::FETCH_ASSOC);
    
      if($user)
         return $user;
      else
         return false;
  }
   /*
    Function : for update  comments basis on comments ID 
    Create   : Braj
    Date     : 30/07/2015

  */
  public function updateComment($array){
    
    $prayId = $array['prayId'];
    $updateResult = $this->_dbh->exec("update comments set comments='".$array['comments']."' where id='".$array['id']."' ");  
      
     
      header('location:showPray.php?id='.$prayId);       
  }
  /*
 
    Function : for select all record from prayerBlock table 
    Create   : Braj
    Date     : 30/07/2015

 */
  public function selectBlockPrayer($id){
    
    $user = $this->_dbh->query("select * from prayerBlock where createPrayUserId='".$id."' ")->fetchAll(PDO::FETCH_ASSOC);
      if($user)
         return $user;
      else
         return false;    
  }
  public function selectUsrName($id){

     $user = $this->_dbh->query("select * from Users where id='".$id."' ")->fetchAll(PDO::FETCH_ASSOC);
      if($user)
         return $user;
      else
         return false;   



  }
public function selectPrayer($id){

     $user = $this->_dbh->query("select * from praytb where id='".$id."' ")->fetchAll(PDO::FETCH_ASSOC);
      if($user)
         return $user;
      else
         return false;   



  }
  public function unBlockPrayUser($id){


    $user = $this->_dbh->exec("DELETE FROM prayerBlock WHERE id = '".$id."'");  
      if($user)
         return true;
      else
         return false;   
  }
  
  /*
    Function  : For Update Block Comments By comments user using given Email Link
    Create    : Braj  
    Date      : 04/08/2015

  */
  
  public function updateBlockComment($array){
    
    $updateResult = $this->_dbh->exec("update comments set comments='".$array['comments']."',encrypt='' where id='".$array['id']."' and encrypt='".$array['encrypt']."' and blockComments='1' ");  
    
    if($updateResult) {?> <script> alert("your comments is successfully updated "); </script> <?php }
    
    else{?><script>alert("you cant use this Link twice or more");</script><?php


    }  
    //header('location:showPray.php?id='.$prayId);  
  }
  public function prayerUser($id){

   $user = $this->_dbh->query("select * from Users where id='".$id."' ")->fetchAll(PDO::FETCH_ASSOC);
      if($user)
         return $user;
      else
         return false;   
  }

}//End of class definition
?>
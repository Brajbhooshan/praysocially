<?php
ini_set('display_errors', 1);
require('./class/init.php'); 

    $redirect    = $init->getRedirect();
    $users       = Users::getInstance();
    $comments    = Comments::getInstance();
    $session     = $init->getSession();
    $session->startSession();

    /* check session */  

    if(!$session->__get("username"))
    {
        $redirect->redirect("index.php");
    }
 
 
    $id          = $session->__get("id");

    /*

    Function  : For select all block_user_comments on behalf of Login_user_id
    Create    : Braj
    Date      : 27/07/2015
    
    */

    $data        = $comments->selectBlockCommentsUser($id);

    $limit       = 6;

    /* calling function of changeImage of login user update */
    
    $updateImage = $users->updateprofile($session->__get("id"));

    if($session->__get("limit"))
    {
        $session->__unset("limit");   
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php 
            include('include/inc.link.php');
            /* start popup for change user profile */
          
             /* include data table file */
            include('include/inc.link-profile.php');
        ?>
        <style>
            .user_pic a:hover {
            color:#fff;
            background:#000 !important;
            }
            .user_pic a {
            background:transparent !important;
            }
            .logo a {outline:none; text-decoration:none !important; border:none;}
        </style>
             <script type="text/javascript">
                        $(document).ready(function($)
                        {
                            $("#example-1").dataTable({
                                aLengthMenu: [
                                    [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
                                ]
                            });
                        });
                        </script>
       
        <script type="text/javascript">
            $(document).ready(function(){

                $(".blockUser").click(function(e){ 
                   
                var text = $(e.target).text();
                if(text == 'Block'){
                    if(confirm("Are sure want to Block this User")){

                        var msg=prompt("Please Enter your message");
                        var txt= $.trim( msg );
                        if(txt){
                            blockUser=this.id;
                            var loginUserId = document.getElementById('loginUserId').value;
                            var uEmail = document.getElementById('uEmail').value;
                           
                            var info='blockUser=' +blockUser;
                            
                            $.ajax({

                                type: "POST",
                                url: "ajaxCall.php",
                                data:{loginUserId: loginUserId,blockUser: blockUser,message: msg,uEmail: uEmail},
                                success:function(data)
                                {
                                    $("#"+blockUser).text("UnBlock");
                                    $("#"+blockUser).removeClass("btn-success").addClass("btn-danger");
                                    alert("User block successfully"); 
                                }
                            });
                        }
                    }
                }
                if(text == 'UnBlock'){
                    if (confirm("Are you sure you want to UnBlock User")) {
                        
                        UnblockUser=this.id;
                        
                        var loginUserId=document.getElementById('loginUserId').value;
                        var uEmail = document.getElementById('uEmail').value;
                        $.ajax({
                            type: "POST",
                            url: "ajaxCall.php",
                            data: {loginUserId: loginUserId,UnblockUser: UnblockUser,uEmail: uEmail},
                            success: function(data)
                            {
                                alert("User UnBlock successfully");
                                $("#"+UnblockUser).text("Block");
                                $("#"+UnblockUser).removeClass("btn-danger").addClass(" btn-success");
                            
                            }
                        });
                    }
                }
                });
            });
        </script>
    </head>
    <body> 
        <?php 
            include('include/header.php');
            include('include/top_banner_home.php');
            include('profile.newheader.php');
        ?>
            <div class="container">
                <div class="row">
                    <div class="">
                          <div class="panel panel-default">
                            <div class="panel-body">
                              
                              
                                <table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>User Name</th>
                                            <th>Comments</th>
                                            <th>Block/Unblock User</th>
                                        </tr>
                                    </thead>
                                
                                    <tfoot>
                                        <tr>
                                            <th>User Name</th>
                                            <th>Comments</th>
                                            <th>Block/Unblock User</th>
                                        </tr>
                                    </tfoot>
                                
                                    <tbody>
                                    <?php 
                                    if(!empty($data)){


                                    foreach ($data as $record) { 
                                        $selectCommentUserName = $comments->selectCommentsUserName($record['commentsUserId']);
                                        //echo "<pre>";print_r($selectCommentUserName);die;
                                    ?>
                                         <tr id="blockrow<?php echo $record['commentsUserId'];?>">
                                            <td>
                                                <a href="unBlockComments.php?commentsUserId=<?php echo $selectCommentUserName[0]['id'] ?>"><?php echo $selectCommentUserName[0]['username'] ?> </a>
                                                <input type="hidden" id="uEmail" name="uEmail" value="<?php echo $selectCommentUserName[0]['email']?>">
                                            </td>
                                            <td><?php echo $record['comment']; ?></td>

                                            <?php if($record['blockUserStatus']==1){ ?>
                                                <td>
                                                    <a id="<?php echo $record['commentsUserId'] ?>" class="btn btn-danger blockUser" values="<?php echo $record['commentsUserId'] ?>" data-toggle="modal" role="button" href="#">UnBlock</a>
                                                </td>
                                                <?php }else{ ?>
                                                <td>
                                                    <a id="<?php echo $record['commentsUserId'] ?>" class="btn btn-success blockUser" values="<?php echo $record['commentsUserId'] ?>" data-toggle="modal" role="button" href="#">Block</a>
                                                    <input type="hidden" value="<?php echo $id;?>" id="loginUserId">
                                                </td>
                                            <?php }?>
                                        </tr>
                                       
                                        <?php }
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>

                </div>
                <?php 
                    /* change profile image popup */
                    include'include/profile_popup.php';
                ?>
            </div>
        </div>
        <?php include('include/footer.php');?>
        <script src="js/ajaxCall.js"></script>
        <link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
        <!-- Bottom Scripts -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/TweenMax.min.js"></script>
        <script src="assets/js/resizeable.js"></script>
        <script src="assets/js/joinable.js"></script>
        <script src="assets/js/xenon-api.js"></script>
        <script src="assets/js/xenon-toggles.js"></script>
        <script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
        <!-- Imported scripts on this page -->
        <script src="assets/js/datatables/dataTables.bootstrap.js"></script>
        <script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
        <script src="assets/js/datatables/tabletools/dataTables.tableTools.min.js"></script>


        <!-- JavaScripts initializations and stuff -->
        <script src="assets/js/xenon-custom.js"></script>
       
    </body>
</html>
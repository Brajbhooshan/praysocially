<?php
$commentsUserId=$_GET['commentsUserId'];
ini_set('display_errors', 1);
require('./class/init.php');

    $redirect = $init->getRedirect();
    $users    = Users::getInstance();
    $comments = Comments::getInstance();
    $session  = $init->getSession();
    $session->startSession();
    
    $updateImage    = $users->updateprofile($session->__get("id"));

    /* check session */  
    if(!$session->__get("username"))
    {
        $redirect->redirect("index.php");
    }
    
    $id       = $session->__get("id");
    $username = $_SESSION['username']; 
    $limit    = 6;
 
    /* calling function for selecting Block Comments from Comments table */ 
    $data     = $comments->selectCommentsData($id,$commentsUserId);
 
 
    if($session->__get("limit"))
    {
      $session->__unset("limit");   
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
          <?php include('include/inc.link.php');?>
        <!--start popup for change user profile-->
      
         <!--include data table file-->

          <?php include('include/inc.link-profile.php');?>
     
    
        <style>
        .user_pic a:hover {
        color:#fff;
        background:#000 !important;
        }
        .user_pic a {
        background:transparent !important;
        }
        .logo a {outline:none; text-decoration:none !important; border:none;}
        </style>

        <!--include data table file-->

  
    </head>
    <body> 
    <?php 
        include('include/header.php');
        include('include/top_banner_home.php');
        include('profile.newheader.php');
    ?>


    <div class="container">
        <div class="">
          
         <div class="profile_box">

    <div class="container">
    <div class="row">
        <div class="">
            <div class="panel panel-default">
                <div class="panel-body">
                     <script>
                       jQuery(document).ready(function()
                        {
                            
                            $(".unblockComments").click(function(e)
                            {    
                                if (confirm("Are you sure you want to UnBlock Comments")) {
                                    unblockComments=this.id;
                                    var info = 'unblockComments=' + unblockComments;
                                    $.ajax({
                                        type: "POST",
                                        url: "ajaxCall.php",
                                        data: info,
                                        success: function(data)
                                        {
                                            $("#"+unblockComments+"").parent().parent().remove();
                                            alert("Comments UnBlock successfully");
                                        }
                                    });
                                }
                            });
                        });
                    </script>
                    <script type="text/javascript">
                        jQuery(document).ready(function($)
                        {
                            $("#example-1").dataTable({
                                aLengthMenu: [
                                    [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
                                ]
                            });
                        });
                    </script>

                    <table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                
                                <th>User Name</th>
                                <th>Pray Title</th>
                                <th>Comments</th>
                                <th>UnBlock</th>
                            </tr>
                        </thead>
                    <!--
                        <tfoot>
                            <tr>
                                <th>user</th>
                                <th>Position</th>
                                <th>Office</th>
                                <th>Age</th>
                                <th>Start date</th>
                                <th>Salary</th>
                            </tr>
                        </tfoot>
                    -->
                        <tbody id="blockUser">
                            <?php if(isset($data) && !empty($data)){foreach($data as $row){ ?>
                            <tr id="blockrow<?php echo $data['id'];?>">
                               
                                <td><?php $selectCommentUserName=$comments->selectCommentsUserName($row['commentsUserId']);foreach ($selectCommentUserName as $name) { ?>

                                <a href="unBlockComments.php?commentsUserId=<?php echo $name['id'] ?>"><?php echo $name['username'] ?> </a>
                                    
                                <?php } ?></td>
                               
                               
                                <td><?php $PrayTitle=$comments->selectPrayTitle($row['prayId']); foreach ($PrayTitle as $prayTitle) { echo $prayTitle['title']; }?>
                                </td>
                                <td>Comments<?php echo $row['comments']?></td>
                                <td ><a id="<?php echo $row['id'];?>" class="btn btn-success unblockComments" values="<?php echo $row['id'];?>" data-toggle="modal" role="button" href="#">Unblock</a></td>
                                
                            </tr>
                            <?php }} else{}?>
                        </tbody>
                    </table>
                </div>
                  </div>
            </div>
            <?php include'include/profile_popup.php';?>
        </div>
      
    </div>
    <?php include('include/footer.php');?>
    <script src="js/ajaxCall.js"></script>
    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
    <!-- Bottom Scripts -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/TweenMax.min.js"></script>
    <script src="assets/js/resizeable.js"></script>
    <script src="assets/js/joinable.js"></script>
    <script src="assets/js/xenon-api.js"></script>
    <script src="assets/js/xenon-toggles.js"></script>
    <script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
    <!-- Imported scripts on this page -->
    <script src="assets/js/datatables/dataTables.bootstrap.js"></script>
    <script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
    <script src="assets/js/datatables/tabletools/dataTables.tableTools.min.js"></script>


    <!-- JavaScripts initializations and stuff -->
    <script src="assets/js/xenon-custom.js"></script>

   
    </body>
</html>

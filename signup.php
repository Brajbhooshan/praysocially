<?php
require('class/init.php'); 
require 'fb/src/config.php';
require 'fb/src/facebook.php';
$session = $init->getSession();
$redirect = $init->getRedirect();
//create object for user class
$users = Users::getInstance();
//Getting redirect object
$session=$init->getSession();
//Starting Session
$session->startSession();
	//check session  
	if($session->__get("username"))
	{
		  $redirect->redirect("index.php");
	}
	
	//Here successfully register new user with valid information
	if(isset($_POST['register'])){
		
		//if password and confirm password is match 
		if($_POST['password']==$_POST['confirm_password']){
			
				$array=array('username'=>$_POST['username'],'password'=>$_POST['password'],'email'=>$_POST['email']);
				$login=$users->registration($array);
			
				if($login==1)
				{?>
					<script>
					alert("New user registration is successfully done !!!");
					window.location.assign('signin.php')</script>
				<?php }
		}
		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<!---Start Head section---->
	<head>
		<m  <?php include('include/inc.link.php');?>
        <!--start popup for change user profile-->
      
         <!--include data table file-->

         
		<script src="js/jquery.validate.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				//hide green chck for chcking username in database 
				$('.text_success').hide();
				$('.text_success1').hide();
				//Here Chcking signup form validation iff all fields remain Empty
				$(".registerfrm").validate({
					rules:
					{
					  username :{
					  required:true,
					  maxlength: 25
					  },
					  email :{
					  required:true,
					  email:true
					  },
					  password :{
					  required:true,
					  maxlength: 25
					  },
					  confirm_password:{
					  equalTo: "#password"
					  },
					   email: {
			                required: true,
			                email: true
			            },
					},
					messages:
					{
					  //email:"Enter Email Address",
					username :{
					required:" Enter username",
					},
					password :{
					required:" Enter password",
					},
				  	confirm_password :"Password does not match, Please try again",
					},
					 email: "Please enter a valid email address",
			  
				});
				//Validation chck iff username is Allready Exist in database 
				$('#label1').hide();
					$('.username').blur(function (e) { 
						e.preventDefault();
						var data = {};
						data.username=$(".username").val();
						$.ajax({
							type: "POST",
							url: "ajaxRegister.php",
							data: data,
							//cache: false,
							success: function (response) {
								//alert(response);
								if(response=="true"){ 
									alert("Username is already exists ! Please try with another");
									
										$(".username").val("");
										$(".username").focus();
										$('.text_success').hide();
										$('#label1').show();
									}
									else if(response=='"emptyname"'){
										
									$('.text_success').hide();
									
								}
								else{
									
										$('#label1').hide();
										$('.text_success').show();
								 }
							}
						}); 
					  return false;
				 });
					$('#email').blur(function (e) { 
						e.preventDefault();
						var data = {};
						data.email=$("#email").val();
						$.ajax({
							type: "POST",
							url: "ajaxRegister.php",
							data: data,
							//cache: false,
							success: function (response) {
								//alert(response);
								if(response=="true"){ alert("Email ID is already exists ! Please try with another");
										$("#email").val("");
										$("#email").focus();
										$('.text_success1').hide();
										$('#label1').show();
									}
									else if(response=='"emptyemail"'){
										
									$('.text_success1').hide();
									
								}
								else{
									
										$('#label1').hide();
										$('.text_success1').show();
								 }
							}
						}); 
					  return false;
				 });
			  });
		
		</script>
	</head>
	<!---End Head section-->
	
	<!---Start Body section  -->
	<body>
		<div class="signup_page">
			<div class="login_register_logo">
				<a href="index.php"><img src="images/logo_login.png" alt="" /></a>
			</div>
			<div class="form_box">
				<h2>Don't have an account!</h2>
				<div class="facebook_btn">
				<a href="https://www.facebook.com/dialog/oauth?client_id=<?php echo $config['App_ID'].'&redirect_uri='.$config['callback_url'].'&scope=email,user_likes,publish_stream';?>"><i class="fa fa-facebook"></i> Sign up with Facebook</a>
					<p>-- or --</p>
				</div>
			   <!---Start new user registration Form--> 
			   <form method="post" id="registerfrm" action="" class="registerfrm">
					<div class="form_inr">
					<div class="form_field">
					  <input type="text" name="username" class="username" placeholder="Username" />
					  <i class="fa fa-check text_success"></i>
					  </div>
					  <div class="form_field">
					  <input type="password" name="password" id="password" class="" placeholder="Password" />
					 <!--- <i class="fa fa-check text_success"></i>-->
					  </div>
					  <div class="form_field">
					  <input type="password" name="confirm_password" class="" placeholder="Confirm Password" />
					 <!-- <i class="fa fa-remove text_danger"></i> -->
					  </div>
					  <div class="form_field">
					  <input type="text" name="email" id="email" class="" placeholder="Email Address" />
					   <i class="fa fa-check text_success1"></i>
					<!--  <i class="fa fa-exclamation text_warning"></i> -->
					  </div>
					  <div class="form_btn">
					  <input type="submit" name="register" class="" value="Sign up" />
					  </div>
					   <p>Already have an account? <a href="signin.php">Login!</a></p>
					</div>
		 
				</form>
				</div>
				<!--End new user registration From-->
			 <?php include('include/footer.php');?>
		</div>
		</body>
</html>
<?php
require('class/init.php'); 

    $redirect       = $init->getRedirect();
    $users          = Users::getInstance();
    $session        = $init->getSession();

    $session->startSession();

    $updateImage    = $users->updateprofile($session->__get("id"));

    /* check session */

    if(!$session->__get("username"))
    {
          $redirect->redirect("index.php");
    }
    
    $limit=6;

    /* calling function for Pray */ 
 
    $prays = $users->viewSuportedPray($session->__get("id"),$limit);
 
    if($session->__get("limit"))
    {
     $session->__unset("limit");   
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head> 
        
        <?php include('include/inc.link.php');?>
        
        <!--start popup for change user profile-->
        
        
        
        <style>
        
            .user_pic a:hover {
            color:#fff;
            background:#000 !important;
            }
            .user_pic a {
            background:transparent !important;
            }
            .logo a {outline:none; text-decoration:none !important; border:none;}
        
        </style>
    </head>
    <body>
        <?php 
            
            include('include/header.php');
            include('include/top_banner_home.php'); 
            include('profile.newheader.php');
        ?>
 
        <div class="container">
        	<div class="prayer_box">
                  <ul class="loadMoreSupportedPrayClass">
                    
                    <!-- show allPray which is pray by login user-->
                    
                    <?php if(!empty($prays)){ foreach ($prays as $pray){ ?>
                    
                    <li>
                        <div class="prayer_counter_box">
                             <div class="praycount_left">
                                <img src="images/hand.png" alt="" />
                                <p><span class="single_pray" id="<?php echo $pray['id'];?>"><?php echo $count=$users->countPray($pray['id']);?> </span>Prayers</p>
                            </div>
                            <div class="creator_name_right">
                                <p>Created By~</p>
                                <br>
                                <p class="creator_name"><?php $author=$users->author($pray['user_id']); ?>
                                   <a href ="author_profile.php?id=<?php echo $author[0]['id']; ?>" ><?php echo $author[0]['username'];  ?></a>
                                </p>
                            </div>
                        </div>
                        <div class="prayer_btn">
                            <a class="Pray like_prayer" id="<?php echo $pray['id'] ?>"  href="javascript:void(0);" value="<?php echo $pray['id'] ?>">+1 Prayer</a>
                            <a href="javascript:void(0);" class="share_fb"><img src="images/fshare.jpg" alt="" /></a>
                            <div class="clear"></div>
                        </div>
                        
                    </li>
                   
                   <?php } }?>
                
                </ul>
                <div class="clear"></div>
                <div class="load_more">
                    <a href="javascript:void(0);" class="loadMoreSupportedPray"><i class="fa fa-refresh"></i> Load More</a>
                    <a href="<?=BASE_URL?>/profile.php"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
                  <?php include'include/profile_popup.php';?>
            </div>
        </div>
  
        <?php include('include/footer.php');?>
        <script src="js/ajaxCall.js"></script>
    </body>
</html>
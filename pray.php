<?php
// include init class
require('class/init.php'); 

//Create object for redirect class
$redirect = $init->getRedirect();

//Create object for object class
$users = Users::getInstance();
$session = $init->getSession();
$session->startSession();

//check session  
if(!$session->__get("username"))
{
      $redirect->redirect("index.php");
}

$username=$_SESSION['username'];

//calling function for selecting userId from users table 
$arrayId=$users->selectUserId($username);

//calling function for selecting allPray from praytb and countPray table
$id=$arrayId['id'];

if((isset($_POST['submit'])) and (!empty($_POST['title'])) and (!empty($_POST['pray'])))
    { 
        $array=array('title'=>$_POST['title'],'pray'=>$_POST['pray'],'user_id'=>$_POST['user_id']);
        $success=$users->pray($array);
        if($success){
                        ?><script>alert("Pray add successfully done !!!");</script><?php
        }
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include('include/inc.link.php');?>
        <script src="ckeditor.js"></script>
        <!--<link rel="stylesheet" href="sample.css">-->
    </head>
    <body>
        <?php 
        include('include/header.php');
        include('include/top_banner_home.php');
        ?>
        <!--body section-->
        <div class="header">

            <div class="container">    
        	   <h2 class="aboutus_content">Prayer</h2>
                <div class="content_left">
                    <form name='addNewPray' id="addNewPray" method="post">
                        <table class="table table-striped">
                            <tr>
                                <td width='70'>Title</td>
                                <td><input name="title" value="" type="text" maxlength="30" class="primarybtn" placeholder="title" required></td>
                                <td><input name="user_id" value="<?php echo $id; ?>" type="hidden" class="primarybtn"/></td>
                            </tr>
                            <tr>
                                <td>Prayer</td>
                                <td><textarea class="ckeditor"  id="editor1" name="pray" value="" required> </textarea></td>
                            </tr>
                            <tr>
                                <td><a href="<?=BASE_URL?>/signup.php" class="primarybtn" width="48">Back</a> </td>
                                <td><input name="submit" value="ADD PRAYER" type="submit" class="secondary"></td>

                            </tr>
                        </table>
                    </form>
                </div>
                <div class="clear"></div>
            </div>
        </div> 
        <?php include('include/footer.php');?>
        <!--footer section-->
   
    </body>
</html>
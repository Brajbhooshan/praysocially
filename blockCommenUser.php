<?php
$session = $init->getSession();
$session->startSession();

 //check session  

    if(!$session->__get("username"))
    {
          $redirect->redirect("index.php");
    }
 
 
$loginUserId=$session->__get("id");

  //Create object for Comments  class
 $comments = Comments::getInstance();

 //calling function of changeImage of login user update
 if(isset($_POST['blockUser']))
 {
 $data=$comments->singleshowUsers($loginUserId,$_POST['blockUser']);
 }else{
 $data=$comments->singleshowUsers($loginUserId,$_POST['UnblockUser']);
}
 foreach ($data as $record) { ?>
                           
    <tr id="blockrow<?php echo $record['commentsUserId'];?>">
        <td><?php $selectCommentUserName=$comments->selectCommentsUserName($record['commentsUserId']);foreach ($selectCommentUserName as $name) { ?>

        <a href="unBlockComments.php?commentsUserId=<?php echo $name['id'] ?>"><?php echo $name['username'] ?> </a>
            
        <?php } ?></td>
        <td><?php echo $record['comment']; ?></td>
        <?php if($record['blockUserStatus']==1){?>
        <td><a id="<?php echo $record['commentsUserId'] ?>" class="btn btn-danger UnblockUser" values="<?php echo $record['commentsUserId'] ?>" data-toggle="modal" role="button" href="#">UnBlock</a></td>
        <?php }else{ ?>
        <td><a id="<?php echo $record['commentsUserId'] ?>" class="btn btn-success blockUser" values="<?php echo $record['commentsUserId'] ?>" data-toggle="modal" role="button" href="#">Block</a></td>
        <?php } ?>
       
    </tr>
<?php } ?>             
   <script>
       jQuery(document).ready(function()
        {
           
             $(".blockUser").click(function(e){ 
                    
                    if(confirm("Are sure want to Block this User")){

                        var msg=prompt("Please Enter your message");
                        var txt= $.trim( msg );
                        if(txt){
                            blockUser=this.id;
                            var loginUserId=document.getElementById('loginUserId').value;
                            var info='blockUser=' +blockUser;
                            $.ajax({

                                type: "POST",
                                url: "ajaxCall.php",
                                data:{loginUserId: loginUserId,blockUser: blockUser,message: msg},
                                success:function(data)
                                {

                                    //localtion.reload();
                                    $("#blockrow"+blockUser+"").replaceWith(data);
                                    alert("User block successfully"); 
                                }
                            });
                        }
                    }
                });
            $(".UnblockUser").click(function(e)
                { 
                    if (confirm("Are you sure you want to UnBlock User")) {
                        UnblockUser=this.id;
                        var loginUserId=document.getElementById('loginUserId').value;
                        $.ajax({
                            type: "POST",
                            url: "ajaxCall.php",
                            data: {loginUserId: loginUserId,UnblockUser: UnblockUser},
                            success: function(data)
                            {
                            $("#blockrow"+UnblockUser+"").replaceWith(data);
                            alert("User UnBlock successfully");
                            }
                        });
                    }
                });
        });
    </script>
<?php
  require('./class/init.php');
    $user       = Users::getInstance();
    $mail       = eMail::getInstance();
    $redirect   = $init->getRedirect();
  if(isset($_POST['success']))
  {

    $email=$_POST['email'];

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) /* Validate email address*/
    {
      $message  = "Invalid email address please type a valid email!!";
    }
    else
    { 
      $result   = $mail->sendMailToUser($email);
        
    }
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <?php include('include/inc.link.php');?>
    <script src="js/jquery.validate.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  </head>
  <body>
    <div class="signin_page">
      <div class="login_register_logo">
        <a href="index.php"><img src="images/logo_login.png" alt="" /></a>
      </div>
      <div class="form_box">
        <h2>Password reset link !</h2>
        <form method="post" action="" id="signinfrm">
          <div class="form_inr">
            <div class="form_field">
              <input id="email" name="email" type="email" placeholder="Email" required>
            </div>
            <div class="form_btn">
            <input type="submit" class="login" value="Send" name="success" />
            </div>
            <p>Back to Signin? <a href="signin.php">Click Now!</a></p></br>
            <p>Don't have an account? <a href="signup.php">Register Now!</a></p>
          </div>
        </form>
      </div>
      <?php include('include/footer.php');?>
    </div>
  </body>
</html>
<div class="profile_box">
        <div class="timeline_big_pic">
        
            <img  src="images/timeline_pic.jpg" alt="" />
        </div>
        <div class="user_details">
            <div class="user_pic">  

                <a href="#link1" title="" class="link" data-toggle="modal">Change profile picture</a>
                <img class="changeimage" style="max-width:160px;" src="<?php if(!empty($updateImage['imageSrc']))echo $updateImage['imageSrc'];else echo "uploads/user_pic.jpg"; ?>"  alt="" />
            </div>
            <div class="user_disc">
            <div class="user_name">
                <?php if($_SESSION['username']){?>
                <p><a href="#"><?php echo $_SESSION['username']; ?></a> <span>Priest</span></p>
                <?php } else{?>
                <p><a href="#"><?php echo "Guest"; ?></a> <span>Priest</span></p>
                <?php }?>
            </div>
            <div class="clear"></div>
            <div class="btn_profile">

                <a href="viewSupportedPray.php?id=<?php echo $session->__get("id");?>" class="active">View supported prayers</a>
                <a href="viewMyPray.php?id=<?php echo $session->__get("id"); ?>" class="active">View my Prayers</a>
                <a href="unBlockPrayer.php" class="active">Unblock Prayers</a>
                <a href="blockUserStatus.php?id=<?php echo $session->__get("id"); ?>" class="active">View Block/Unblock User</a>
                <a href="myfollower.php?id=<?php echo $session->__get("id"); ?>" class="active">View My Followers</a>
            </div>
            </div>
        </div>
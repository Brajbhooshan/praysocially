<?php
ini_set("display_errors","on");
require('class/init.php');
//object for session class
    $session=$init->getSession();
    //Starting Session
    $session->startSession();
	$user = Users::getInstance();

	$path = "uploads/";

	$valid_formats = array("jpg", "png", "gif", "bmp","JPEG","JPG", "PNG", "GIF", "BMP","jpeg");
		if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
		{   //print_r($_FILES);
			$id=$session->__get("id");
			$name = $_FILES['file']['name'];
			$size = $_FILES['file']['size'];
			if(strlen($name))
			{
				list($txt, $ext) = explode(".", $name);
				if(in_array($ext,$valid_formats))
				{
					if($size<(1024*1024)) // Image size max 1 MB
					{ 
						 $actual_image_name = time().$id.".".$ext;
						 $tmp = $_FILES['file']['tmp_name'];
						 $targetPath=$path.$actual_image_name;	
							
							if(move_uploaded_file($tmp, $path.$actual_image_name))
							{
								//calling function of uploadUserimage for changeImage of login user
								$result=$user->uploadUserImage($targetPath,$actual_image_name,$id);
								
								if($result){
									echo json_encode ($result);
								}
								else{

									echo json_encode($result);
								}
								
							}
							else
							echo "failed";
					}
					else
					echo "Image file size max 1 MB";
					}
				else
				echo "Invalid file format..";
			}
			else
			echo "Please select image..!";
			exit;
		}
?>
<?php
ini_set('display_errors', 1);
// include init class
require('./class/init.php'); 

//Create object for redirect class
 $redirect = $init->getRedirect();
 //Create object for object class
 $users = Users::getInstance();

  //Create object for admin  class
 $admin = Admin::getInstance();
//die("kk");

  //getting Username from SESSION
 $session = $init->getSession();
 $session->startSession();

 //check session  
    if(!$session->__get("username"))
    {
          $redirect->redirect("index.php");
    }
 $username=$_SESSION['username']; 
 $limit=6;
 //calling function for selecting userId from users table 
 $arrayId=$users->selectUserId($username);

 //calling function for selecting allPray from praytb and countPray table for login user
 $id=$arrayId['id'];
 $prays=$users->allPrayLoginUser($id,$limit);
  
 //calling function of changeImage of login user update
 $updateImage=$users->updateprofile($id);
 //echo "<pre>";print_r($updateImage);die("dff");
 //echo $updateImage['imageSrc']; die("///h");

 //create object for session class
 $session=$init->getSession();
 //Starting Session
 //$session->startSession();
 if($session->__get("limit"))
 {
  $session->__unset("limit");   
 }
 //calling function of changeImage of login user update
 $data=$admin->showUsers();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>:: Praysocially ::</title>
        <link href="css/style.css" type="text/css" rel="stylesheet" />
        <link href="css/responsive.css" type="text/css" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,400italic,500,500italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <!--start popup for change user profile-->
        <link href="popup/css/bootstrap.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
        <script src="popup/js/bootstrap.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
    
        <style>
        .user_pic a:hover {
        color:#fff;
        background:#000 !important;
        }
        .user_pic a {
        background:transparent !important;
        }
        .logo a {outline:none; text-decoration:none !important; border:none;}
        </style>

        <!--include data table file-->

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
    <link rel="stylesheet" href="assets/css/fonts/linecons/css/linecons.css">
    <link rel="stylesheet" href="assets/css/fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/xenon-core.css">
    <link rel="stylesheet" href="assets/css/xenon-forms.css">
    <link rel="stylesheet" href="assets/css/xenon-components.css">
    <link rel="stylesheet" href="assets/css/xenon-skins.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <script src="assets/js/jquery-1.11.1.min.js"></script>
    </head>
    <body> 
    <?php 
        include('include/header.php');
        include('include/top_banner_home.php');
    ?>
    <div class="container">
        <div class="">
            <div class="panel panel-default">
                <div class="panel-body">
                    <script type="text/javascript">
                        jQuery(document).ready(function($)
                        {
                            $("#example-1").dataTable({
                                aLengthMenu: [
                                    [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
                                ]
                            });
                        });
                    </script>
                    <table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Delete</th>
                                <th>Block</th>
                            </tr>
                        </thead>
                    <!--
                        <tfoot>
                            <tr>
                                <th>user</th>
                                <th>Position</th>
                                <th>Office</th>
                                <th>Age</th>
                                <th>Start date</th>
                                <th>Salary</th>
                            </tr>
                        </tfoot>
                    -->
                        <tbody id="blockUser">
                            <?php foreach($data as $row){
                                ?>
                            <tr>
                                <td><?php echo $row['id']?></td>
                                <td><?php echo $row['username']?></td>
                                <td><?php echo $row['email']?></td>
                                <td><a id="<?php echo $row['id'];?>" class="btn btn-danger remove" values="<?php echo $row['id'];?>" data-toggle="modal" role="button" href="#">Delete</a></td>
                                <?php if($row['block']==1){?>
                                <td><a id="<?php echo $row['id'];?>" class="btn btn-success unblock" values="<?php echo $row['id'];?>" data-toggle="modal" role="button" href="#">Unblock</a></td>
                                <?php }else{?>    
                                <td><a id="<?php echo $row['id'];?>" class="btn btn-danger block" values="<?php echo $row['id'];?>" data-toggle="modal" role="button" href="#">Block</a></td>
                             <?php }?>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!--start change image popup div-->
        <div>
            <div id="link1" class="modal fade bs-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                         <div class="modal-header ">
                            <!--start uploadImage Form-->
                            <form id="uploadImage" class="forminfo" action="" method="post" enctype="multipart/form-data">
                                <div id="selectImage">
                                    <label>Select Your Image</label><br/>
                                    <input type="file" name="file" id="file" required />
                                    <input type="hidden" name="user_id" value="<?php echo $id;?>"/>
                                    <input type="submit" name="uploadImage" value="Upload" class="primarybtn submit" />
                                </div>
                            </form>
                            <!--End uploadImage Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <!--End change image popup div-->  
    </div>
    <?php include('include/footer.php');?>
    <script src="js/ajaxCall.js"></script>
    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
    <!-- Bottom Scripts -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/TweenMax.min.js"></script>
    <script src="assets/js/resizeable.js"></script>
    <script src="assets/js/joinable.js"></script>
    <script src="assets/js/xenon-api.js"></script>
    <script src="assets/js/xenon-toggles.js"></script>
    <script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
    <!-- Imported scripts on this page -->
    <script src="assets/js/datatables/dataTables.bootstrap.js"></script>
    <script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
    <script src="assets/js/datatables/tabletools/dataTables.tableTools.min.js"></script>


    <!-- JavaScripts initializations and stuff -->
    <script src="assets/js/xenon-custom.js"></script>

    <script>
       jQuery(document).ready(function()
        {
            var remove_id;
            $(".remove").click(function(e)
            {
                if (confirm("Are you sure you want to Delete")) {
                    remove_id=this.id;
                    var info = 'remove=' + remove_id;
                    $.ajax({
                        type: "POST",
                        url: "ajaxCall.php",
                        data: info,
                        success: function(data)
                        {
                        alert("Recorde Delete successfully");
                        //alert(data);
                             $("#"+remove_id+"").parent().parent().remove();
                        }
                    });
                }
            });
            $(".block").click(function(e)
            {
                if (confirm("Are you sure you want to Block User")) {
                        var msg = prompt("Please enter your Message");
                        var txt = $.trim( msg );
                        if(txt){
                        block=this.id;
                        var info = 'block=' + block;
                        $.ajax({
                            type: "POST",
                            url: "ajaxCall.php",
                            data: { block: block, message: msg },
                            success: function(data)
                            {
                            alert("Recorde Block successfully");
                            $('#blockUser  tbody').append( data );
                            }
                        });
                    }
                }
            });
            $(".unblock").click(function(e)
            {
                if (confirm("Are you sure you want to UnBlock User")) {
                    unblock=this.id;
                    var info = 'unblock=' + unblock;
                    $.ajax({
                        type: "POST",
                        url: "ajaxCall.php",
                        data: info,
                        success: function(data)
                        {
                        alert("Recorde Un Block successfully");
                        //$("#block").html(data);
                        //$("#"+remove_id+"").parent().parent().remove();
                        }
                    });
                }
            });
        });
    </script>
    </body>
</html>

<?php
// include init class
require('class/init.php'); 

//Create object for redirect class
$redirect = $init->getRedirect();

//Create object for object class
$users = Users::getInstance();

//create object for mail class
$mail = eMail::getInstance();

$session = $init->getSession();
$session->startSession();
//check session  
if((!$session->__get("username")) || (!isset($_GET['id'])))
{   if(isset($_GET['action']) && !empty($_GET['action']) && ($_GET['action'] == 'editpray')) {
        $session->__set("editpray","true");
        $session->__set("editprayid",$_GET['id']);
        $redirect->redirect("signin.php");
    }else{
        $redirect->redirect("index.php");
    }
}
if($session->__get("id"))

    $pray_id=$_GET['id'];  
    $pray_detail=$users->showPrayByid($pray_id);
  
if($session->__get("id")!=$pray_detail['user_id'])
{
     $redirect->redirect("index.php");
}
if(isset($_POST['submit'])){


if ( (!empty($_POST['title'])) and (!empty($_POST['pray'])) )
    { 
        if(empty($_POST['check']))
        $array=array('title'=>$_POST['title'],'pray'=>$_POST['pray']);
        else
        $array=array('title'=>$_POST['title'],'pray'=>$_POST['pray'] , 'status' => $_POST['check']);
        $table="praytb";
        $where=array("id"=>$pray_id);
        $update=$users->update($table,$array,$where);
        if(!empty($update)) { 
            // if check box button is click than send mail to admin to confirmation mail
            if(!empty($_POST['check'])) {

                $user=$users->userdetail($pray_detail['blocked_by']);
                //send mail to admin for unblock pray
                $send=$mail->unblockMail ($user[0]['email'],$_POST['title']);
                if ($send) {
                    ?>
                    <script>alert("Prayer Update successfully  And waiting for responce  !!!");
                    window.location.href='viewMyPray.php?id=<?php echo $pray_id; ?>';</script><?php
                }
        
            }else{
                ?>
                <script>alert("Prayer Update successfully  !!!");
                window.location.href='viewMyPray.php?id=<?php echo $pray_id; ?>';</script><?php
            }

        }
    }
    else{
       ?><script>alert("Can't Empty Any Field  !!!");</script><?php 
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include('include/inc.link.php');?>
        <script src="ckeditor.js"></script>
        <!--<link rel="stylesheet" href="sample.css">-->
    </head>
    <body>
        <?php 
        include('include/header.php');
        include('include/top_banner_home.php');
        ?>
        <!--body section-->
        <div class="header">

            <div class="container">    
        	   <h2 class="aboutus_content">Prayer</h2>
                <div class="content_left">
                
                    <form name='UpdatePray' id="UpdatePray" method="post">
                        <table class="table table-striped">
                            <tr>
                            
                                <td width='70'>Title</td>
                                <td><input name="title" type="text" maxlength="30" class="primarybtn" value="<?php echo $pray_detail['title']; ?>" placeholder="title" required></td>
                                <td><input name="user_id" value="<?php echo $id; ?>" type="hidden" class="primarybtn"/></td>
                            </tr>

                            <tr>
                                <td>Prayer</td>
                                <td><textarea class="ckeditor" id="editor1" name="pray"  required><?php echo $pray_detail['pray']; ?> </textarea></td>
                            </tr>
                            <?php if($pray_detail['status'] == 1) { ?>
                            <tr>
                                <td>click here for Unblock Prayer</td>
                                <td><input type="checkbox" name="check" value="2" ></td>

                            </tr>
                            <?php } ?>
                            <tr>
                                <td><a href="<?=BASE_URL?>/profile.php" class="primarybtn" width="48">Back</a> </td>
                                <td><input name="submit" value="UPDATE PRAYER" type="submit" class="secondary"></td>

                            </tr>
                        </table>
                    </form>
                    
                </div>
                <div class="clear"></div>
            </div>
        </div> 
        <?php include('include/footer.php');?>
        <!--footer section-->
       
    </body>
</html>

<?php 
  //Create object for admin  class
 $admin = Admin::getInstance();

 //calling function of changeImage of login user update
 if(isset($_POST['block']))
 {
 $data=$admin->singleshowUsers($_POST['block']);
 }else{
 $data=$admin->singleshowUsers($_POST['unblock']);
}
 foreach($data as $row){ ?>
<tr id="blockrow<?php echo $row['id'];?>">
    <td><?php echo $row['id']?></td>
    <td><a href="adminprayer.php?userinfo=<?php echo $row['id']?>" class="adminusername"><?php echo $row['username']?></a></td>
    <td><?php echo $row['email']?></td>
    <td><a id="<?php echo $row['id'];?>" class="btn btn-danger remove" values="<?php echo $row['id'];?>" data-toggle="modal" role="button" href="#">Delete</a></td>
    <?php if($row['block']==1){?>
    <td><a id="<?php echo $row['id'];?>" class="btn btn-success unblock" values="<?php echo $row['id'];?>" data-toggle="modal" role="button" href="#">Unblock</a></td>
    <?php }else{?>    
    <td><a id="<?php echo $row['id'];?>" class="btn btn-danger block" values="<?php echo $row['id'];?>" data-toggle="modal" role="button" href="#">Block</a></td>
 <?php }?>
</tr>
<?php } ?>
                     <script>
       jQuery(document).ready(function()
        {
            var remove_id;
            $(".remove").click(function(e)
            {
                if (confirm("Are you sure you want to Delete")) {
                    remove_id=this.id;
                    var info = 'remove=' + remove_id;
                    $.ajax({
                        type: "POST",
                        url: "ajaxCall.php",
                        data: info,
                        success: function(data)
                        {
                        
                        $("#"+remove_id+"").parent().parent().remove();
                        alert("Recorde Delete successfully");
                        }
                    });
                }
            });
            $(".block").click(function(e)
            {
                if (confirm("Are you sure you want to Block User")) {
                        var msg = prompt("Please enter your Message");
                        var txt = $.trim( msg );
                        if(txt){
                        block=this.id;
                        var info = 'block=' + block;
                        $.ajax({
                            type: "POST",
                            url: "ajaxCall.php",
                            data: { block: block, message: msg },
                            success: function(data)
                            {
                                $("#blockrow"+block+"").replaceWith(data);
                                alert("User Block successfully");
                            }
                        });
                    }
                }
            });
            $(".unblock").click(function(e)
            {
                if (confirm("Are you sure you want to UnBlock User")) {
                    unblock=this.id;
                    var info = 'unblock=' + unblock;
                    $.ajax({
                        type: "POST",
                        url: "ajaxCall.php",
                        data: info,
                        success: function(data)
                        {
                        $("#blockrow"+unblock+"").replaceWith(data);
                        alert("User UnBlock successfully");
                        }
                    });
                }
            });
        });
    </script>